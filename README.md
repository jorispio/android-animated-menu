
## Instructions
### Common and generic setup
```
        <com.jto.android.animatedmenu.RotaryMenu
            android:id="@+id/explodeMenu"
            android:layout_width="match_parent"
            android:layout_height="match_parent"
            app:fab_radius="@dimen/big_circle_radius"
            app:gap_between_menu_fab="@dimen/min_gap"
            app:menu_items="@menu/menu"
            app:menu_radius="@dimen/small_circle_radius"
            app:min_height="240dp"
            app:fab_color="@color/colorPrimaryDark"
            app:menu_color="@color/blue"
            app:menu_darkcolor="@color/colorPrimaryDark"/>
```
Different shapes are available depending on the style you are looking for, namely: Star, Rotary, Rudder, Linear.

If you have set everything through XML, simply use:
```
        View view = inflater.inflate(layout, viewGroup, false);
        AnimatedMenu mMenu = view.findViewById(R.id.explodeMenu);
        mMenu.setOnMenuListener(this);
```
The purpose is to declare the menu events listener.

The parent fragment/activity implements interface IMenuExplode

```
public interface IMenuExplode {
    void onMenuOpen();
    void onMenuClose();
    void onMenuItemClicked(int id);
}
```

### Advanced settings
#### Specific XML parameters
Note:
- the "with_labels" attribute print the button labels
- the "app:direction" attribute specifies the expanding/collapsing direction, when applicable.
- "menu_button_background" allows the specify a drawable for the button instead of using the default look (plain color). This can also be modified by setting through code the button renderer. The button renderer shall implement IButtonBitmap interface.

#### Setting parameters dynamically
Some parameters can be settup through code rather than the XML file.
Through code:            
```
View view = inflater.inflate(layout, viewGroup, false);
ExplodeMenu mMenu = view.findViewById(R.id.explodeMenu);

mMenu.setOnMenuListener(DemoFragmentCodeOnly.this)
                                    .getMenuInflater().inflate(R.menu.menu, mMenu);

mMenu.getRenderer()
          .setGravity(Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL);
          .setColor(getContext().getResources().getColor(R.color.colorPrimary))
          .setColorDark(getContext().getResources().getColor(R.color.colorPrimaryDark))
          .withLabels(withLabels.isChecked())
          .setMenuButtonRadius(50).setButtonMargin(10);

mMenu.getRenderer().setMainMenuButtonRadius(60);
          .setMainMenuButtonColor(getContext().getResources().getColor(R.color.colorPrimary));

mMenu.build();        
mMenu.init();
```


### Examples
#### Star

<img src="screenshots/star.gif"/>

```
<com.jto.android.animatedmenu.StarMenu
            android:id="@+id/explodeMenu"
            android:layout_width="match_parent"
            android:layout_height="match_parent"
            android:layout_gravity="bottom|center_horizontal"
            app:min_height="240dp"
            app:menu_items="@menu/menu"
            app:fab_radius="40dp"
            app:fab_background="@color/colorPrimaryDark"
            app:gap_between_menu_fab="100dp"
            app:menu_button_radius="25dp"
            app:menu_color="@color/colorPrimary"
            app:menu_darkcolor="@color/colorPrimaryDark"/>
```

#### Rotary

<img src="screenshots/rotary.gif"/>

```
<com.jto.android.animatedmenu.RotaryMenu
            android:id="@+id/explodeMenu"
            android:layout_width="match_parent"
            android:layout_height="match_parent"
            android:layout_gravity="center_horizontal|center_vertical"
            app:fab_radius="40dp"
            app:gap_between_menu_fab="100dp"
            app:menu_items="@menu/menu"
            app:menu_button_radius="25dp"
            app:min_height="240dp"
            app:fab_background="@color/colorPrimaryDark"
            app:menu_color="@color/colorPrimary"
            app:menu_darkcolor="@color/colorPrimaryDark"/>
```

#### Corner
<img src="screenshots/corner.gif"/>
```
<com.jto.android.animatedmenu.CornerMenu
            android:id="@+id/explodeMenu"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:layout_gravity="bottom|start"
            app:fab_radius="40dp"
            app:gap_between_menu_fab="100dp"
            app:menu_button_margin="18dp"
            app:menu_button_radius="25dp"
            app:menu_button_rearrange="true"
            app:menu_buttons_rows="3"
            app:menu_items="@menu/bigmenu"
            app:min_height="600dp" />
```


#### Arc
<img src="screenshots/arc.gif" />
```
<com.jto.android.animatedmenu.StarMenu
            android:id="@+id/explodeMenu"
            android:layout_width="match_parent"
            android:layout_height="match_parent"
            android:layout_gravity="bottom|center_horizontal"
            app:with_fab="false"
            app:gap_between_menu_fab="160dp"
            app:arc_eccentricity="0.3"
            app:menu_items="@menu/bigmenu"
            app:menu_button_radius="60dp"
            app:menu_color="@color/colorPrimary"
            app:menu_darkcolor="@color/colorPrimaryDark"
            app:min_height="240dp"
            app:arc_angle_start="30"
            app:arc_angle_stop="150" />
```

#### Rudder

<img src="screenshots/rudder.gif"/>

```
<com.jto.android.animatedmenu.RudderMenu
            android:id="@+id/explodeMenu"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:layout_gravity="center_horizontal|center_vertical"
            app:direction="HORIZONTAL"
            app:fab_background="@color/colorPrimaryDark"
            app:fab_radius="30dp"
            app:gap_between_menu_fab="40dp"
            app:menu_button_background="#55F607"
            app:menu_button_margin="8dp"
            app:menu_button_radius="25dp"
            app:menu_color="@color/colorPrimary"
            app:menu_darkcolor="@color/colorPrimaryDark"
            app:menu_items="@menu/menu"
            app:min_height="240dp" />
```

Note

#### Linear (or Rudder without Floating Action Button)

<img src="screenshots/linear.gif" width=200/>

```            
<com.jto.android.animatedmenu.LinearMenu
            android:id="@+id/explodeMenu"
            android:layout_width="match_parent"
            android:layout_height="match_parent"
            android:layout_gravity="center_horizontal|bottom"
            app:menu_button_background="@drawable/custom_button"
            app:with_labels="true"
            app:menu_items="@menu/menu"
            app:menu_button_radius="25dp"
            app:min_height="240dp"
            app:button_label_text_size="12dp"
            app:direction="HORIZONTAL"/>
```
Note:
- the "app:direction" attribute specifies the expanding/collapsing direction.
- for all menu, the attribute "app:menu_button_background" can be used to set custom drawable for menu buttons


## Download and Installation

Declare the repository (if not already done):
```
repositories {
    maven {
        url "https://jitpack.io"
    }
}
```

Then add the dependency:
```
dependencies {
    implementation 'com.gitlab.jorispio:android-animated-menu:1.0.0@aar'
}
```

[![](https://jitpack.io/v/com.gitlab.jorispio/android-animated-menu.svg)](https://jitpack.io/#com.gitlab.jorispio/android-animated-menu)


## License

The MIT License

Copyright (c) 2018-2019 Joris OLYMPIO

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
