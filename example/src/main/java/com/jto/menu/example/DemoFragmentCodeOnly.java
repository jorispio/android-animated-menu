package com.jto.menu.example;

import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.jto.android.animatedmenu.AnimatedMenu;
import com.jto.android.animatedmenu.IMenuExplode;
import com.jto.android.animatedmenu.renderers.LinearExplodeMenuDrawFunction;
import com.jto.android.animatedmenu.renderers.RotaryMenuDrawFunction;
import com.jto.android.animatedmenu.renderers.RudderMenuDrawFunction;
import com.jto.android.animatedmenu.renderers.StarExplodeMenuDrawFunction;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class DemoFragmentCodeOnly extends Fragment implements IMenuExplode {
    private AnimatedMenu mMenu;
    private CheckBox withLabels;

    public DemoFragmentCodeOnly() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup viewGroup, Bundle savedInstanceState){
        View view = inflater.inflate(R.layout.layout_demo_code, viewGroup, false);

        Spinner spinner = view.findViewById(R.id.menuType);
        if (spinner != null) {
            HashMap<String, String> data = new HashMap<>();

            List<String> list = new ArrayList<>();
            list.add("StarExplodeMenuDrawFunction");
            list.add("RudderMenuDrawFunction");
            list.add("RotaryMenuDrawFunction");
            list.add("LinearExplodeMenuDrawFunction");
            ArrayAdapter<String> dataAdapter = new ArrayAdapter<>(this.getContext(),
                    android.R.layout.simple_spinner_item, list);
            dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

            spinner.setAdapter(dataAdapter);
            spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    setMenuStyle(position);
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        }

        withLabels = view.findViewById(R.id.with_labels);

        Button applyButton = view.findViewById(R.id.apply_button);
        if (applyButton != null) {
            applyButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mMenu.setOnMenuListener(DemoFragmentCodeOnly.this)
                            .getMenuInflater().inflate(R.menu.menu, mMenu);
                    mMenu.getRenderer()
                            .setLayoutGravity(Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL)
                            .setColor(getContext().getResources().getColor(R.color.colorPrimary))
                            .setColorDark(getContext().getResources().getColor(R.color.colorPrimaryDark))
                            .withLabels(withLabels.isChecked())
                            .setMenuButtonRadius(50).setButtonMargin(10);

                    if (mMenu.getRenderer().isWithFloatingActionButton()) {
                        mMenu.getRenderer().setMainMenuButtonRadius(60)
                                .setMainMenuButtonColor(getContext().getResources().getColor(R.color.colorPrimary));
                    }
                    mMenu.build();
                    mMenu.invalidate();
                }
            });
        }

        mMenu = view.findViewById(R.id.explodeMenu);

        int mMenuButtonColor = getResources().getColor(com.jto.android.animatedmenu.R.color.default_color);
        int mMenuButtonDarkColor = getResources().getColor(com.jto.android.animatedmenu.R.color.default_color_dark);

        mMenu.setOnMenuListener(this)
                .getMenuInflater().inflate(R.menu.menu, mMenu);

        mMenu.getRenderer().setButtonMargin(10)
                        .setColor(mMenuButtonColor)
                        .setColorDark(mMenuButtonDarkColor)
                        .setMainMenuButtonRadius(60)
                        .setMenuButtonRadius(40)
                        .withLabels(false);
        mMenu.build();

        return view;
    }

    private void setMenuStyle(int position) {
        if (position == 0) {
            mMenu.setRenderer(new StarExplodeMenuDrawFunction().setArcRadius(120).withFloatingActionButton(true));
        } else if (position == 1) {
            mMenu.setRenderer(new RudderMenuDrawFunction().setDirection(RudderMenuDrawFunction.RudderDirection.HORIZONTAL).setButtonDistance(80).withFloatingActionButton(true));
        } else if (position == 2) {
            mMenu.setRenderer(new RotaryMenuDrawFunction().setArcRadius(120).withFloatingActionButton(true));
        } else if (position == 3) {
            mMenu.setRenderer(new LinearExplodeMenuDrawFunction().setButtonDistance(80).withFloatingActionButton(false));
        }
    }

    @Override
    public void onMenuOpen() {
        showToast("Menu Open");
    }

    @Override
    public void onMenuClose() {
        showToast( "Menu Close");
    }

    @Override
    public void onMenuItemClicked(int id) {
        showToast( "Menu item clicked : " + id);
    }

    private void showToast(String msg){
        Toast.makeText(getContext(), msg, Toast.LENGTH_SHORT).show();
    }
}
