package com.jto.menu.example;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.jto.android.animatedmenu.AnimatedMenu;
import com.jto.android.animatedmenu.buttons.GradientButtonBitmap;

public class DemoFragmentWithButtonRenderer extends DemoFragment{
    private int layout;
    private AnimatedMenu mMenu;

    public DemoFragmentWithButtonRenderer() {
    }

    public DemoFragmentWithButtonRenderer setLayout(int layout) {
        this.layout = layout;
        return this;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup viewGroup, Bundle savedInstanceState){

        int mButtonsColor = getContext().getResources().getColor(R.color.default_color);
        int mButtonsDarkColor = getContext().getResources().getColor(R.color.default_color_dark);

        View view = inflater.inflate(layout, viewGroup, false);
        mMenu = view.findViewById(R.id.explodeMenu);
        mMenu.setOnMenuListener(this)
                .getMenuInflater().inflate(R.menu.menu, mMenu);
        mMenu.getRenderer().setButtonRenderer(new GradientButtonBitmap(getContext())
                .setColor(mButtonsColor)
                .setBorderColor(mButtonsDarkColor));
        //mMenu.build();

        return view;
    }

    @Override
    public void onMenuOpen() {
        showToast("Menu Open");
    }

    @Override
    public void onMenuClose() {
        showToast( "Menu Close");
    }

    @Override
    public void onMenuItemClicked(int id) {
        showToast( "Menu item clicked : " + id);
        mMenu.close();
    }

    private void showToast(String msg){
        Toast.makeText(getContext(), msg, Toast.LENGTH_SHORT).show();
    }
}
