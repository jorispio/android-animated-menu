package com.jto.menu.example;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

class TabsAdapter extends FragmentStatePagerAdapter {
    private final static int[] layouts = new int[]{
            R.layout.layout_demo1,
            R.layout.layout_demo2_rotary,
            R.layout.layout_demo3_rudder,
            R.layout.layout_demo4_arc,
            R.layout.layout_demo5_linear,
            R.layout.layout_demo6_corner_xml,
            R.layout.layout_demo_code};

    private int mNumOfTabs;

    public TabsAdapter(FragmentManager fm, int NoofTabs){
        super(fm);
        this.mNumOfTabs = NoofTabs;
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }

    @Override
    public Fragment getItem(int position){
        if (layouts[position] == R.layout.layout_demo2_rotary) {
            // demo using XML and a button renderer
            return new DemoFragmentWithButtonRenderer().setLayout(layouts[position]);
        }
        else if (layouts[position] == R.layout.layout_demo5_linear) {
            // demo using XML and a button drawable
            return new DemoFragment().setLayout(layouts[position]);
        }
        if (layouts[position] == R.layout.layout_demo6_corner_xml) {
            // demo using XML only
            return new DemoFragmentXmlOnly().setLayout(layouts[position]);
        }
        else if (layouts[position] == R.layout.layout_demo_code) {
            // demo code only
            return new DemoFragmentCodeOnly();
        }

        // demo using XML only, but different menu template
        return new DemoFragment().setLayout(layouts[position]);
    }
}