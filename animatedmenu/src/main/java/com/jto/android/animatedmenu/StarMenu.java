package com.jto.android.animatedmenu;

import android.content.Context;
import android.util.AttributeSet;

import com.jto.android.animatedmenu.renderers.StarExplodeMenuDrawFunction;

public class StarMenu extends AnimatedMenu {
    public StarMenu(Context context) {
        this(context, null);
    }

    public StarMenu(Context context, AttributeSet attrs) {
        this(context, attrs, 0, 0);
    }

    public StarMenu(Context context, AttributeSet attrs, int defStyleAttr) {
        this(context, attrs, defStyleAttr, 0);
    }

    public StarMenu(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes, new StarExplodeMenuDrawFunction(), true);
    }
}
