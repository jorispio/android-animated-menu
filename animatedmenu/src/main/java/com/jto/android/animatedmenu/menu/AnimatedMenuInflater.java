package com.jto.android.animatedmenu.menu;

import android.content.Context;
import android.content.res.XmlResourceParser;
import android.util.AttributeSet;
import android.util.Xml;
import android.view.InflateException;

import androidx.annotation.NonNull;

import com.jto.android.animatedmenu.AnimatedMenu;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;

/** Menu inflater, following the same convention of #MenuInflater.
 * Menu groups are not handled.
 */
public class AnimatedMenuInflater {
    private static final String XML_MENU = "menu";
    private static final String XML_GROUP = "group";
    private static final String XML_ITEM = "item";

    private final Context mContext;

    public AnimatedMenuInflater(Context mContext) {
        this.mContext = mContext;
    }

    public AnimatedMenuInflater inflate(int menuResourceId, @NonNull AnimatedMenu animatedMenu) {
        XmlResourceParser parser = null;
        try {
            parser = mContext.getResources().getLayout(menuResourceId);
            AttributeSet attrs = Xml.asAttributeSet(parser);

            parseMenu(parser, attrs, animatedMenu);
        } catch (XmlPullParserException | IOException e) {
            throw new InflateException("Error inflating menu XML", e);
        } finally {
            if (parser != null) parser.close();
        }

        animatedMenu.completeMenu();
        return this;
    }

    private void parseMenu(@NonNull XmlResourceParser parser, @NonNull final AttributeSet attrs,
                           @NonNull AnimatedMenu menu) throws XmlPullParserException, IOException {
        final MenuState menuState = new MenuState(mContext, menu);

        menu.clearMenuItems();

        int eventType = parser.getEventType();
        String tagName;
        boolean lookingForEndOfUnknownTag = false;
        String unknownTagName = null;

        // This loop will skip to the menu start tag
        do {
            if (eventType == XmlPullParser.START_TAG) {
                tagName = parser.getName();
                if (tagName.equals(XML_MENU)) {
                    // Go to next tag
                    eventType = parser.next();
                    break;
                }

                throw new RuntimeException("Expecting menu, got " + tagName);
            }
            eventType = parser.next();
        } while (eventType != XmlPullParser.END_DOCUMENT);

        boolean reachedEndOfMenu = false;
        while (!reachedEndOfMenu) {
            switch (eventType) {
                case XmlPullParser.START_TAG:
                    if (lookingForEndOfUnknownTag) {
                        break;
                    }

                    tagName = parser.getName();
                    if (tagName.equals(XML_GROUP)) {
                        menuState.readGroup(attrs);
                    } else if (tagName.equals(XML_ITEM)) {
                        menuState.readItem(attrs);
                    } else if (tagName.equals(XML_MENU)) {
                        throw new RuntimeException("SubMenu not allowed!");
                    } else {
                        lookingForEndOfUnknownTag = true;
                        unknownTagName = tagName;
                    }
                    break;

                case XmlPullParser.END_TAG:
                    tagName = parser.getName();
                    if (lookingForEndOfUnknownTag && tagName.equals(unknownTagName)) {
                        lookingForEndOfUnknownTag = false;
                        unknownTagName = null;
                    } else if (tagName.equals(XML_GROUP)) {
                        ///menuState.resetGroup();
                    } else if (tagName.equals(XML_ITEM)) {
                        if (!menuState.hasAddedItem()) {
                            menuState.addItem();
                            //registerMenu(menuState.addItem(), attrs);
                        }
                    } else if (tagName.equals(XML_MENU)) {
                        reachedEndOfMenu = true;
                    }
                    break;

                case XmlPullParser.END_DOCUMENT:
                    throw new RuntimeException("Unexpected end of document");
            }

            eventType = parser.next();
        }
    }
}
