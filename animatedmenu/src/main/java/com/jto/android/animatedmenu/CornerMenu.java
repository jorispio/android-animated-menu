package com.jto.android.animatedmenu;

import android.content.Context;
import android.util.AttributeSet;

import com.jto.android.animatedmenu.renderers.CornerMenuDrawFunction;

public class CornerMenu extends AnimatedMenu {
    public CornerMenu(Context context) {
        this(context, null);
    }

    public CornerMenu(Context context, AttributeSet attrs) {
        this(context, attrs, 0, 0);
    }

    public CornerMenu(Context context, AttributeSet attrs, int defStyleAttr) {
        this(context, attrs, defStyleAttr, 0);
    }

    public CornerMenu(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes, new CornerMenuDrawFunction(), true);
    }
}
