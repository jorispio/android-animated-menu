package com.jto.android.animatedmenu.buttons;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.LayerDrawable;
import android.graphics.drawable.RippleDrawable;

import androidx.annotation.ColorInt;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.jto.android.animatedmenu.R;

import java.util.ArrayList;

public class ButtonRenderer implements IButtonRenderer {
    private static final String TAG = "MENU";

    protected int mMenuButtonRadius;
    protected int mTextMargin = 10;
    private float mButtonLabelTextSize = 32f;
    private Paint mTextPaintBackground;
    private Paint mTextPaint;
    private boolean labelVisibility;

    private Paint mPaint;
    private Paint mBorderPaint;
    private Drawable background;
    private Bitmap bmp = null;
    private int bmpHeight;
    private int bmpWidth;

    private boolean withLabels;
    private Rect bounds = new Rect();
    private boolean hasDrawable;
    ArrayList<GradientDrawable> mGradientDrawable = new ArrayList<>();

    public ButtonRenderer(@NonNull Context context) {
        mTextPaint = new Paint();
        mTextPaint.setTextSize(mButtonLabelTextSize);
        mTextPaint.setColor(Color.BLACK);
        mTextPaint.setTextAlign(Paint.Align.CENTER);

        mTextPaintBackground = new Paint(mTextPaint);
        mTextPaintBackground.setColor(Color.WHITE);
        mTextPaintBackground.setStyle(Paint.Style.FILL);

        mPaint = new Paint();
        mPaint.setColor(context.getResources().getColor(R.color.default_color));
        mPaint.setStyle(Paint.Style.FILL_AND_STROKE);

        mBorderPaint = new Paint(mPaint);
        mBorderPaint.setStyle(Paint.Style.STROKE);
        mBorderPaint.setStrokeWidth(1f);
        mBorderPaint.setColor(context.getResources().getColor(R.color.default_color_dark));

        labelVisibility = true;
    }

    public boolean isLabelVisibille() {
        return labelVisibility;
    }

    public ButtonRenderer setColor(@ColorInt int color) {
        mPaint.setColor(color);
        return this;
    }

    public ButtonRenderer setBorderColor(@ColorInt int color) {
        mBorderPaint.setColor(color);
        return this;
    }

    @Override
    public ButtonRenderer setLabelTextSize(float textSize) {
        mButtonLabelTextSize = textSize;
        mTextPaint.setTextSize(mButtonLabelTextSize);
        return this;
    }

    @Override
    public ButtonRenderer setLabelTextMargin(int textMargin) {
        mTextMargin = textMargin;
        return this;
    }

    @Override
    public ButtonRenderer setLabelBackgroundColor(int color) {
        mTextPaintBackground.setColor(color);
        return this;
    }

    private void processDrawable(Drawable drawable) {
        if (drawable instanceof BitmapDrawable) {
            bmp = ((BitmapDrawable) drawable).getBitmap();
            bmpWidth = bmp.getWidth();
            bmpHeight = bmp.getHeight();

        } else if (drawable instanceof LayerDrawable) {
            LayerDrawable layer = ((LayerDrawable) drawable);
            int nbLayers = layer.getNumberOfLayers();
            for (int i=0; i<nbLayers; ++i) {
                Drawable dra = layer.getDrawable(i);
                processDrawable(dra);
            }

        } else if (drawable instanceof RippleDrawable) {
            //FIXME
            RippleDrawable ripple = (RippleDrawable) drawable;
            int nbStates = ripple.getNumberOfLayers();

            for (int i=0; i<nbStates; ++i) {
                Drawable dra = ripple.getDrawable(i);
                processDrawable(dra);
            }

        } else if (drawable instanceof GradientDrawable) {
            GradientDrawable gradient = ((GradientDrawable) drawable.mutate());
            gradient.setBounds(0, 0, mMenuButtonRadius, mMenuButtonRadius);
            mGradientDrawable.add(gradient);
            hasDrawable = true;
        }
    }

    @Override
    public IButtonRenderer setBackground(Drawable mMenuButtonBackground) {
        this.background = mMenuButtonBackground;
        hasDrawable = (background != null);

        if (hasDrawable) {
            if (background instanceof ColorDrawable) {
                setColor(((ColorDrawable) background).getColor());
                hasDrawable = false; // don't need it anymore
            } else {
                processDrawable(background);
            }
        }
        return this;
    }

    @Nullable
    @Override
    public Drawable getDrawable() {
        return background;
    }

    @Override
    public ButtonRenderer setRadius(int mMenuButtonRadius) {
        this.mMenuButtonRadius = mMenuButtonRadius;
        return this;
    }

    @Override
    public ButtonRenderer withLabels(boolean withLabels) {
        this.withLabels = withLabels;
        return this;
    }


    @Override
    public void draw(Canvas canvas, float x, float y, String text) {
        if (hasDrawable) {
            if (bmp != null) {
                Rect src = new Rect(0, 0, bmpWidth, bmpHeight);
                Rect dest = new Rect((int)(x - mMenuButtonRadius / 2f),
                        (int)(y - mMenuButtonRadius / 2f),
                        (int)(x + mMenuButtonRadius / 2f),
                        (int)(y + mMenuButtonRadius / 2f));
                canvas.drawBitmap(bmp, src, dest, mPaint);
            }

            // FIXME do not draw mask
            for (GradientDrawable dr : mGradientDrawable) {
                dr.setBounds((int)x-mMenuButtonRadius, (int)y-mMenuButtonRadius,
                        (int)x + (int)mMenuButtonRadius, (int)y + (int)mMenuButtonRadius);
                dr.draw(canvas);
            }

        } else {
            // default button drawing
            canvas.drawCircle(x, y, mMenuButtonRadius, mPaint);
            canvas.drawCircle(x, y, mMenuButtonRadius, mBorderPaint);
        }

        drawText(canvas, x, y, text);
    }

    protected void drawText(Canvas canvas, float x, float y, String text) {
        if (withLabels && labelVisibility) {
            mTextPaint.getTextBounds(text, 0, text.length(), bounds);

            // show label bottom of the button
            y += mMenuButtonRadius + bounds.height() + 2 * mTextMargin;
            bounds.offset((int)x - bounds.width()/2, (int)y);

            bounds.inset(-mTextMargin, -mTextMargin);
            RectF roundRect = new RectF(bounds);
            canvas.drawRoundRect(roundRect, 5f, 5f, mTextPaintBackground);
            canvas.drawText(text, x, y, mTextPaint);
        }
    }

    @Override
    public void hideLabels() {
        labelVisibility = false;
    }

    @Override
    public void showLabels() {
        labelVisibility = true;
    }
}
