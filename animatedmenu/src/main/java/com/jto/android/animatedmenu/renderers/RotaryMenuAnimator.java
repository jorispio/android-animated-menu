package com.jto.android.animatedmenu.renderers;

import android.animation.ObjectAnimator;
import android.view.animation.AnticipateOvershootInterpolator;

import com.jto.android.animatedmenu.buttons.MenuButtonArea;

import java.util.ArrayList;

public class RotaryMenuAnimator extends StarExplodeMenuAnimator {
    private AbstractMenuDrawFunction mMenuRenderer;

    RotaryMenuAnimator(AbstractMenuDrawFunction renderer) {
        super(renderer);
        mMenuRenderer = renderer;
    }

    @Override
    public void updateAnimations(ArrayList<MenuButtonArea> menuPoints, int radius) {
        mShowAnimation.clear();
        mHideAnimation.clear();

        int mNumberOfMenu = menuPoints.size();
        for (int i = 0; i < mNumberOfMenu; i++) {
            float paramMaxValue = menuPoints.get(i).getRadius();

            ObjectAnimator animShow = ObjectAnimator.ofFloat(menuPoints.get(i), "Parameter", 0f, paramMaxValue);
            animShow.setDuration(animationDuration);
            animShow.setInterpolator(new AnticipateOvershootInterpolator());
            animShow.setStartDelay(0);
            animShow.addUpdateListener(mUpdateListener);
            animShow.addListener(mMenuRenderer.getOnShowListener());
            mShowAnimation.add(animShow);

            ObjectAnimator animHide = animShow.clone();
            animHide.setFloatValues(paramMaxValue, 0f);
            animHide.setStartDelay(0);
            animHide.removeAllListeners();
            animShow.addUpdateListener(mUpdateListener);
            animHide.addListener(mMenuRenderer.getOnHideListener());
            mHideAnimation.add(animHide);
        }
    }
}
