package com.jto.android.animatedmenu.renderers;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.jto.android.animatedmenu.buttons.ButtonRenderer;
import com.jto.android.animatedmenu.buttons.MenuButtonArea;

public class LinearExplodeMenuDrawFunction extends AbstractMenuDrawFunction<LinearExplodeMenuDrawFunction> {
    private RudderMenuDrawFunction.RudderDirection mDirection;

    public LinearExplodeMenuDrawFunction() {
        super(false);
        animator = new LinearExplodeMenuAnimator(this);
        mDirection = RudderMenuDrawFunction.RudderDirection.HORIZONTAL;
        withFloatingActionButton(false);
    }

    @Override
    public void init(@NonNull Context context, @NonNull View view, @Nullable AttributeSet attrs, Resources resources) {
        super.init(context, view, attrs, resources);

        init(context, view);
    }

    @Override
    public void init(@NonNull Context context, @NonNull View view) {
        super.init(context, view);
        animator.init(view);

        mButtonRenderer = new ButtonRenderer(context);
        ((ButtonRenderer) mButtonRenderer).setColor(mButtonsColor)
                .setBorderColor(mButtonsDarkColor)
                .setBackground(mMenuButtonBackground)
                .setRadius(mMenuButtonRadius)
                .withLabels(mWithLabels).hideLabels();
    }


    /** Compute the current button position from the animated parameter */
    @Override
    protected Point getButtonPosition(MenuButtonArea menuButtonArea) {
        float parameter = menuButtonArea.getParameter();
        float x = getXCoordinate(parameter);
        float y = getYCoordinate(parameter);
        return new Point((int)x, (int)y);
    }

    private float getXCoordinate(double parameter) {
        float x;
        if (mDirection == RudderMenuDrawFunction.RudderDirection.HORIZONTAL) {
            x = (float) parameter;

        } else {
            x = (float) 0;
        }
        return x;
    }

    private float getYCoordinate(double parameter) {
        float y;
        if (mDirection == RudderMenuDrawFunction.RudderDirection.HORIZONTAL) {
            y = (float) 0;

        } else {
            y = (float) parameter;
        }

        return y;
    }

    @Override
    public void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);

        for (int i = 0; i < getNumberOfMenu(); i++) {
            MenuButtonArea menuButtonArea = new MenuButtonArea();
            menuButtonArea.setRadius(mDistanceToFab).setParameter(mDistanceToFab);
            menuButtonArea.setAngle((float) (Math.PI / (getNumberOfMenu() + 1f)) * (i + 1));
            mMenuPoints.add(menuButtonArea);

            if (mDrawableArray != null) {
                for (Object drawable : getDrawableArray()) {
                    if (drawable != null) {
                        ((Drawable)drawable).setBounds(0, 0, mMenuButtonRadius, mMenuButtonRadius);
                    }
                }
            }
        }

        animator.updateAnimations(getMenuPoints(), getMenuButtonRadius());
        onResume();
    }

    @Override
    public int onTouch(MotionEvent event) {
        for (int i = 0; i < mMenuPoints.size(); i++) {
            MenuButtonArea menuButtonArea = mMenuPoints.get(i);
            float parameter = menuButtonArea.getParameter();
            float x = parameter + getCenterX();
            float y = (float) getCenterY() - 0;
            if (menuButtonArea.isClicked(x, y, event, mMenuButtonRadius)) {
                    return getMenuItems().get(i).getItemId();
            }
        }
        return -1;
    }
}
