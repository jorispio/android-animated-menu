package com.jto.android.animatedmenu.renderers;

import android.animation.ObjectAnimator;
import android.view.animation.AccelerateDecelerateInterpolator;

import com.jto.android.animatedmenu.buttons.MenuButtonArea;

import java.util.ArrayList;

public class LinearExplodeMenuAnimator extends AbstractShowOnlyAnimator {
    private static final long DEFAULT_ANIMATION_DURATION = 1000;

    LinearExplodeMenuAnimator(AbstractMenuDrawFunction renderer) {
        super(renderer);
        setAnimationDuration(DEFAULT_ANIMATION_DURATION);
    }

    @Override
    public void updateAnimations(ArrayList<MenuButtonArea> menuPoints, int radius) {
        mShowAnimation.clear();

        final int mNumberOfMenu = menuPoints.size();
        final float centerIndex = (mNumberOfMenu - 1) / 2f + 1;

        int margin = mMenuRenderer.getButtonMargin();

        for (int i = 0; i < mNumberOfMenu; i++) {
            float parameter = (centerIndex - (float)(i + 1)) * (float)(2 * radius + margin);
            ObjectAnimator animShow = ObjectAnimator.ofFloat(menuPoints.get(i), "Parameter", 0f, parameter);
            animShow.setDuration(animationDuration);
            animShow.setInterpolator(new AccelerateDecelerateInterpolator());
            animShow.setStartDelay(0);
            animShow.addUpdateListener(mUpdateListener);
            animShow.addListener(mMenuRenderer.getOnShowListener());
            mShowAnimation.add(animShow);
        }
    }
}
