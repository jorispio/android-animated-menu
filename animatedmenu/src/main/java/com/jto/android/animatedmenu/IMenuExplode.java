package com.jto.android.animatedmenu;

public interface IMenuExplode {
    void onMenuOpen();
    void onMenuClose();
    void onMenuItemClicked(int id);
}
