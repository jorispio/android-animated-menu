package com.jto.android.animatedmenu.renderers;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class CornerMenuDrawFunction extends GenericMenuDrawFunction {
    private Paint mCirclePaint;

    public CornerMenuDrawFunction() {
        super();
        animator = new RotaryMenuAnimator(this);
        setAngularRange(0, Math.PI / 2);
        setRows(2);

        mCirclePaint = new Paint();
        mCirclePaint.setStyle(Paint.Style.STROKE);
        mCirclePaint.setStrokeWidth(4f);
    }

    public void init(@NonNull Context context, @NonNull View view, @Nullable AttributeSet attrs, Resources resources) {
        super.init(context, view, attrs, resources);

        // set the expansion direction accordingly with layout gravity
        int hGravity = getLayoutGravity() & Gravity.HORIZONTAL_GRAVITY_MASK;
        int vGravity = getLayoutGravity() & Gravity.VERTICAL_GRAVITY_MASK;
        if (hGravity == Gravity.START) {
            if (vGravity == Gravity.BOTTOM) {
                setAngularRange(-Math.PI / 2, 0);
            } else {
                setAngularRange(0, Math.PI / 2);
            }
        } else if (hGravity == Gravity.END) {
            if (vGravity == Gravity.BOTTOM) {
                setAngularRange(Math.PI / 2., Math.PI);
            } else{
                setAngularRange(Math.PI, 3 * Math.PI / 2.);
            }
        }

        mCirclePaint.setColor(mButtonsDarkColor);
    }

    @Override
    public void draw(Canvas canvas) {
        mCirclePaint.setColor(mButtonsDarkColor);
        for (int iRow = 0; iRow < getRows(); ++iRow) {
            float radius = getRowCurrentRadius(iRow);
            canvas.drawArc(getCenterX() - radius, getCenterY() - radius, getCenterX() + radius, getCenterY() + radius,
                    (float) getAngularRangeStart() - (float)getAngularRangeSweep(), (float) getAngularRangeSweep(),
                    false, mCirclePaint);
        }
        super.draw(canvas);
    }
}
