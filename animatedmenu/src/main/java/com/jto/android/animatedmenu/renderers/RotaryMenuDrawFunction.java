package com.jto.android.animatedmenu.renderers;

import android.graphics.Point;

import com.jto.android.animatedmenu.buttons.MenuButtonArea;

public class RotaryMenuDrawFunction extends GenericMenuDrawFunction {

    public RotaryMenuDrawFunction() {
        super();
        animator = new RotaryMenuAnimator(this);
        setAngularRange(-Math.PI,  Math.PI);
    }

    @Override
    protected Point getButtonPosition(MenuButtonArea menuButtonArea) {
        float parameter = menuButtonArea.getParameter();
        float t = (float) (parameter / menuButtonArea.getRadius() * 2 * Math.PI);
        float x = (float) (parameter * Math.cos(t +  menuButtonArea.getAngle()));
        float y = (float) (parameter * Math.sin(t +  menuButtonArea.getAngle()));
        return new Point((int)x, (int)y);
    }
}
