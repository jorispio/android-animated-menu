package com.jto.android.animatedmenu.buttons;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RadialGradient;
import android.graphics.Shader;
import android.graphics.drawable.Drawable;

import androidx.annotation.ColorInt;
import androidx.annotation.NonNull;

public class GradientButtonBitmap extends ButtonRenderer {
    @NonNull
    private final Paint paint;
    private final Paint paintWhite;
    private final Paint ringPaint;
    @NonNull
    private final Paint paintProxiPt;
    private float cx;
    private float cy;

    @ColorInt private int mainColor;

    public GradientButtonBitmap(Context context) {
        super(context);
        paint = new Paint();
        paintWhite = new Paint();
        ringPaint = new Paint();
        paintProxiPt = new Paint();
        setPaint();
    }

    private void setPaint() {
        paint.setStyle(Paint.Style.STROKE);
        paint.setColor(Color.GRAY);
        paint.setTextSize(18);
        paint.setFakeBoldText(true);
        paint.setAntiAlias(true);
        paint.setTextAlign(Paint.Align.CENTER);

        paintWhite.setColor(Color.WHITE);

        ringPaint.setAntiAlias(true);
        ringPaint.setStyle(Paint.Style.STROKE);
        ringPaint.setAntiAlias(true);

        paintProxiPt.setAntiAlias(true);
        paintProxiPt.setStyle(Paint.Style.FILL);
        paintProxiPt.setAntiAlias(true);
    }


    public GradientButtonBitmap setColor(@ColorInt int color) {
        mainColor = color;
        return this;
    }

    @Override
    public IButtonRenderer setBackground(Drawable mMenuButtonBackground) {
        return this;
    }

    @Override
    public void draw(@NonNull Canvas canvas, float x, float y, String text) {
        updateGradient(x, y);
        canvas.drawCircle(x, y, mMenuButtonRadius, ringPaint);

        drawText(canvas, x, y, text);
    }

    private void updateGradient(float x, float y) {
        ringPaint.setStrokeWidth(10f + mMenuButtonRadius * 0.25f);

        int color0 = 0x0FDEDEDF;
        int color1 = mainColor;

        float stop = 0.3f;
        float stop2 = stop + (1 - stop) * .10f;
        float stop3 = stop + (1 - stop) * .32f;
        float stop4 = stop + (1 - stop) * .64f;
        float stop5 = stop + (1 - stop) * .90f;
        float stop6 = 1f;

        RadialGradient radialGradient = new RadialGradient(x, y,
                mMenuButtonRadius,
                new int[]{color0, color1, color0, color0, color1, color1},
                new float[]{stop, stop2, stop3, stop4, stop5, stop6},
                Shader.TileMode.CLAMP);
        ringPaint.setShader(radialGradient);
    }

    static
    public int getColors(int index) {
        if (index > 5) {
            index = 0;
        }
        return GradientButtonBitmap.getColors()[index];
    }

    public static int[] getColors() {
        return new int[]{Color.BLUE, Color.CYAN, Color.GREEN, Color.MAGENTA, Color.RED, Color.YELLOW};
    }
}
