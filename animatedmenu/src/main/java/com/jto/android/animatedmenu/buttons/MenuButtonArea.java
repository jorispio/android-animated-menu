package com.jto.android.animatedmenu.buttons;

import android.view.MotionEvent;

public class MenuButtonArea {
    private float x;
    private float y;
    private float radius = 0f;
    private float angle = 0f;
    private float parameter = 0f;

    public MenuButtonArea() {

    }

    public void setX(float x1) {
        x = x1;
    }

    public float getX() {
        return x;
    }

    public void setY(float y1) {
        y = y1;
    }

    public float getY() {
        return y;
    }

    public MenuButtonArea setRadius(float r) {
        radius = r;
        parameter = r;
        return this;
    }

    public float getRadius() {
        return radius;
    }

    /** Set the value of varying parameter for animation, using ObjectAnimator.
     * @return*/
    public MenuButtonArea setParameter(float r) {
        parameter = r;
        return this;
    }

    /** Get the value of varying parameter for animation */
    public float getParameter() {
        return parameter;
    }

    /** Set the value of the polar angle of the button position in polar space
     * @param angle, rad
     * @return this
     */
    public MenuButtonArea setAngle(float angle) {
        this.angle = angle;
        return this;
    }

    /** Get the value of the polar angle of the button position in polar space */
    public float getAngle() {
        return angle;
    }

    private void update() {
        x = (float) getRadius() * (float) Math.cos(getAngle());
        y = (float) getRadius() * (float) Math.sin(getAngle());
    }

    /**
     * Evaluate whether the button was clicked.
     *
     * @param x absolute x-position of the button
     * @param y absolute y-position of the button
     * @param event motion event
     * @return true if the button area was clicked.
     */
    public boolean isClicked(float x, float y, MotionEvent event, float btnRadius) {
        if (event.getX() >= x - btnRadius && event.getX() <= x + btnRadius) {
            if (event.getY() >= y - btnRadius && event.getY() <= y + btnRadius) {
                return true;
            }
        }
        return false;
    }
}
