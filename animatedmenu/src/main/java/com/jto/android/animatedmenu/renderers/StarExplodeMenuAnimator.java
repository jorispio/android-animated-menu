package com.jto.android.animatedmenu.renderers;

import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.view.View;
import android.view.animation.AnticipateOvershootInterpolator;

import androidx.annotation.NonNull;

import com.jto.android.animatedmenu.buttons.MenuButtonArea;

import java.util.ArrayList;

public class StarExplodeMenuAnimator implements IMenuAnimator {
    private static final long DEFAULT_ANIMATION_DURATION = 1000;

    private View view;
    private AbstractMenuDrawFunction mMenuRenderer;

    protected ArrayList<ObjectAnimator> mShowAnimation = new ArrayList<>();
    protected ArrayList<ObjectAnimator> mHideAnimation = new ArrayList<>();

    protected long animationDuration;

    StarExplodeMenuAnimator(AbstractMenuDrawFunction renderer) {
        setAnimationDuration(DEFAULT_ANIMATION_DURATION);
        this.mMenuRenderer = renderer;
    }

    public StarExplodeMenuAnimator setAnimationDuration(long animationDuration) {
        this.animationDuration = animationDuration;
        return this;
    }

    @Override
    public void init(@NonNull View view) {
        this.view = view;
    }

    @Override
    public void clear() {
        mHideAnimation.clear();
        mHideAnimation = null;
        mShowAnimation.clear();
        mShowAnimation = null;
    }

    @Override
    public boolean startShowAnimate() {
        for (ObjectAnimator objectAnimator : mShowAnimation) {
            objectAnimator.start();
        }
        return true;
    }

    @Override
    public boolean startHideAnimate() {
        for (ObjectAnimator objectAnimator : mHideAnimation) {
            objectAnimator.start();
        }
        return true;
    }

    @Override
    public void cancelAllAnimation() {
        for (ObjectAnimator objectAnimator : mHideAnimation) {
            objectAnimator.cancel();
        }
        for (ObjectAnimator objectAnimator : mShowAnimation) {
            objectAnimator.cancel();
        }
    }

    protected ValueAnimator.AnimatorUpdateListener mUpdateListener = new ValueAnimator.AnimatorUpdateListener() {
        @Override
        public void onAnimationUpdate(ValueAnimator valueAnimator) {
            view.invalidate();
        }
    };

    @Override
    public void updateAnimations(ArrayList<MenuButtonArea> menuPoints, int radius) {
        mShowAnimation.clear();
        mHideAnimation.clear();
        int mNumberOfMenu = menuPoints.size();
        for (int i = 0; i < mNumberOfMenu; i++) {
            float paramMaxValue = menuPoints.get(i).getRadius();

            ObjectAnimator animShow = ObjectAnimator.ofFloat(menuPoints.get(i), "Parameter", 0f, paramMaxValue);
            animShow.setDuration(animationDuration);
            animShow.setInterpolator(new AnticipateOvershootInterpolator());
            animShow.setStartDelay((animationDuration * (mNumberOfMenu - i)) / 10);
            animShow.addUpdateListener(mUpdateListener);
            animShow.addListener(mMenuRenderer.getOnShowListener());
            mShowAnimation.add(animShow);

            ObjectAnimator animHide = animShow.clone();
            animHide.setFloatValues(paramMaxValue, 0f);
            animHide.setStartDelay((animationDuration * i) / 10);
            animHide.removeAllListeners();
            animShow.addUpdateListener(mUpdateListener);
            animHide.addListener(mMenuRenderer.getOnHideListener());
            mHideAnimation.add(animHide);
        }
    }
}
