package com.jto.android.animatedmenu.renderers;

import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.view.View;
import android.view.animation.AnticipateOvershootInterpolator;

import androidx.annotation.NonNull;

import com.jto.android.animatedmenu.buttons.MenuButtonArea;

import java.util.ArrayList;

public class AbstractShowOnlyAnimator implements IMenuAnimator {
    private static final long DEFAULT_ANIMATION_DURATION = 1000;

    protected View view;
    protected AbstractMenuDrawFunction mMenuRenderer;
    protected ArrayList<ObjectAnimator> mShowAnimation = new ArrayList<>();

    protected long animationDuration;

    public AbstractShowOnlyAnimator(@NonNull AbstractMenuDrawFunction renderer) {
        setAnimationDuration(DEFAULT_ANIMATION_DURATION);
        this.mMenuRenderer = renderer;
    }

    public AbstractShowOnlyAnimator setAnimationDuration(long animationDuration) {
        this.animationDuration = animationDuration;
        return this;
    }

    @Override
    public void init(@NonNull View view) {
        this.view = view;
    }

    @Override
    public void clear() {
        mShowAnimation.clear();
        mShowAnimation = null;
    }

    @Override
    public boolean startShowAnimate() {
        for (ObjectAnimator objectAnimator : mShowAnimation) {
            objectAnimator.start();
        }
        return true;
    }

    @Override
    public boolean startHideAnimate() {
        return false;
    }

    @Override
    public void cancelAllAnimation() {
        for (ObjectAnimator objectAnimator : mShowAnimation) {
            objectAnimator.cancel();
        }
    }

    protected ValueAnimator.AnimatorUpdateListener mUpdateListener = new ValueAnimator.AnimatorUpdateListener() {
        @Override
        public void onAnimationUpdate(ValueAnimator valueAnimator) {
            view.invalidate();
        }
    };

    @Override
    public void updateAnimations(ArrayList<MenuButtonArea> menuPoints, int radius) {
        mShowAnimation.clear();

        int mNumberOfMenu = menuPoints.size();
        for (int i = 0; i < mNumberOfMenu; i++) {
            ObjectAnimator animShow = ObjectAnimator.ofFloat(menuPoints.get(i), "Parameter", 0f, 1f);
            animShow.setDuration(animationDuration);
            animShow.setInterpolator(new AnticipateOvershootInterpolator());
            animShow.setStartDelay(0);
            animShow.addUpdateListener(mUpdateListener);
            animShow.addListener(mMenuRenderer.getOnShowListener());
            mShowAnimation.add(animShow);
        }
    }
}
