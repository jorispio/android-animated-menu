package com.jto.android.animatedmenu.renderers;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.jto.android.animatedmenu.R;
import com.jto.android.animatedmenu.buttons.ButtonRenderer;
import com.jto.android.animatedmenu.buttons.MenuButtonArea;

public class RudderMenuDrawFunction extends AbstractMenuDrawFunction<RudderMenuDrawFunction> {

    public enum RudderDirection {
        HORIZONTAL(0), VERTICAL(1);

        int value;
        RudderDirection(int value) {
            this.value = value;
        }
        public static RudderDirection valueOf(int value) {
            if (value == 0) return HORIZONTAL;
            return VERTICAL;
        }
    }

    private Paint mRudderPaint;
    private int mFabColor;
    private RudderDirection mDirection;

    public RudderMenuDrawFunction() {
        super(true);
        animator = new RudderMenuAnimator(this);
        mDirection = RudderDirection.HORIZONTAL;
    }

    public RudderMenuDrawFunction setDirection(RudderDirection direction) {
        this.mDirection = direction;
        return this;
    }

    @Override
    public void init(@NonNull Context context, @NonNull View view, @Nullable AttributeSet attrs, Resources resources) {
        super.init(context, view, attrs, resources);

        mFabColor = mButtonsColor;
        if (attrs != null) {
            TypedArray a = context.getTheme().obtainStyledAttributes(
                    attrs,
                    R.styleable.AnimatedMenu,
                    0, 0);

            mFabColor = a.getColor(R.styleable.AnimatedMenu_fab_background, resources.getColor(R.color.default_color_dark));

            if (a.hasValue(R.styleable.AnimatedMenu_direction)) {
                int value = a.getInt(R.styleable.AnimatedMenu_direction, 0);
                mDirection = RudderDirection.valueOf(value);
            }

            a.recycle();
        }

        init(context, view);
    }

    @Override
    public void init(@NonNull Context context, @NonNull View view) {
        super.init(context, view);

        mRudderPaint = new Paint();
        mRudderPaint.setColor(mFabColor);
        mRudderPaint.setStyle(Paint.Style.FILL_AND_STROKE);

        animator.init(view);

        mButtonRenderer = new ButtonRenderer(context);
        ((ButtonRenderer) mButtonRenderer).setColor(mButtonsColor)
                .setBorderColor(mButtonsDarkColor)
                .setBackground(mMenuButtonBackground)
                .setRadius(mMenuButtonRadius)
                .withLabels(mWithLabels);
    }

    /** Compute the current button position from the animated parameter */
    @Override
    protected Point getButtonPosition(MenuButtonArea menuButtonArea) {
        float parameter = menuButtonArea.getParameter();
        float x = getXCoordinate(parameter);
        float y = getYCoordinate(parameter);
        return new Point((int)x, (int)y);
    }

    @Override
    public void draw(Canvas canvas) {
        if (!initialised) return;

        float maxParameter = 0;
        float minParameter = 100;
        for (int i = 0; i < getNumberOfMenu(); i++) {
            MenuButtonArea menuButtonArea = mMenuPoints.get(i);
            float parameter = menuButtonArea.getParameter();
            maxParameter = Math.max(maxParameter, parameter);
            minParameter = Math.min(minParameter, parameter);
        }
        maxParameter += mMenuButtonRadius + (this.getMainMenuButtonRadius() - mMenuButtonRadius);
        minParameter -= mMenuButtonRadius + (this.getMainMenuButtonRadius() - mMenuButtonRadius);

        canvas.save();
        canvas.translate(getCenterX(), getCenterY());
        Path path = createPath(minParameter, maxParameter);
        canvas.drawPath(path, mRudderPaint);
        canvas.restore();

        // place it here to draw on top (and hide) the menu buttons
        super.draw(canvas);
    }

    private float getXCoordinate(double parameter) {
        float x;
        if (mDirection == RudderDirection.HORIZONTAL) {
            x = (float) parameter + getPaddingStart();

        } else {
            x = (float) getPaddingStart();
        }
        return x;
    }

    private float getYCoordinate(double parameter) {
        float y;
        if (mDirection == RudderDirection.HORIZONTAL) {
            y = (float) getPaddingTop();

        } else {
            y = (float) parameter + getPaddingTop();
        }

        return y;
    }

    private Path createPath(float xmin, float xmax) {
        Path path = new Path();
        path.addRoundRect(xmin, mFloatingActionButtonRadius,
                xmax, -mFloatingActionButtonRadius,
                mFloatingActionButtonRadius, mFloatingActionButtonRadius,
                Path.Direction.CW);
        return path;
    }

    @Override
    public void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);

        for (int i = 0; i < getNumberOfMenu(); i++) {
            MenuButtonArea menuButtonArea = new MenuButtonArea();
            menuButtonArea.setRadius(mMenuButtonRadius + mMenuButtonsMargin)
                           .setParameter(mMenuButtonRadius + mMenuButtonsMargin);
            mMenuPoints.add(menuButtonArea);
        }

        if (mDrawableArray != null) {
            for (Object drawable : getDrawableArray()) {
                if (drawable != null) {
                    ((Drawable)drawable).setBounds(0, 0, mMenuButtonRadius, mMenuButtonRadius);
                }
            }
        }

        animator.updateAnimations(getMenuPoints(), getMenuButtonRadius());
        onResume();

    }

    @Override
    public int onTouch(MotionEvent event) {
        for (int i = 0; i < mMenuPoints.size(); i++) {
            MenuButtonArea menuButtonArea = mMenuPoints.get(i);
            float parameter = menuButtonArea.getParameter();
            float x = getXCoordinate(parameter) + getCenterX();
            float y = getYCoordinate(parameter) + getCenterY();
            if (menuButtonArea.isClicked(x, y, event, mMenuButtonRadius)) {
                return getMenuItems().get(i).getItemId();
            }
        }
        return -1;
    }
}
