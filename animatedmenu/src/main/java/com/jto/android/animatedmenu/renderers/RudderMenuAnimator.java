package com.jto.android.animatedmenu.renderers;

import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.view.View;
import android.view.animation.AnticipateOvershootInterpolator;

import androidx.annotation.NonNull;

import com.jto.android.animatedmenu.buttons.MenuButtonArea;

import java.util.ArrayList;

public class RudderMenuAnimator implements IMenuAnimator {
    private static final long DEFAULT_ANIMATION_DURATION = 1000;

    private View view;
    private ArrayList<ObjectAnimator> mShowAnimation = new ArrayList<>();
    private ArrayList<ObjectAnimator> mHideAnimation = new ArrayList<>();

    private long animationDuration;
    private AbstractMenuDrawFunction mMenuRenderer;

    RudderMenuAnimator(@NonNull RudderMenuDrawFunction rudderMenuDrawFunction) {
        setAnimationDuration(DEFAULT_ANIMATION_DURATION);
        this.mMenuRenderer = rudderMenuDrawFunction;
    }

    @Override
    public RudderMenuAnimator setAnimationDuration(long animationDuration) {
        this.animationDuration = animationDuration;
        return this;
    }

    @Override
    public void init(@NonNull View view) {
        this.view = view;
    }

    @Override
    public void clear() {
        mShowAnimation.clear();
        mShowAnimation = null;
        mHideAnimation.clear();
        mHideAnimation = null;
    }

    @Override
    public boolean startShowAnimate() {
        for (ObjectAnimator objectAnimator : mShowAnimation) {
            objectAnimator.start();
        }
        return true;
    }

    @Override
    public boolean startHideAnimate() {
        for (ObjectAnimator objectAnimator : mHideAnimation) {
            objectAnimator.start();
        }
        return true;
    }

    @Override
    public void cancelAllAnimation() {
        for (ObjectAnimator objectAnimator : mShowAnimation) {
            objectAnimator.cancel();
        }
        for (ObjectAnimator objectAnimator : mHideAnimation) {
            objectAnimator.cancel();
        };
    }

    private ValueAnimator.AnimatorUpdateListener mUpdateListener = new ValueAnimator.AnimatorUpdateListener() {
        @Override
        public void onAnimationUpdate(ValueAnimator valueAnimator) {
            view.invalidate();
        }
    };

    @Override
    public void updateAnimations(ArrayList<MenuButtonArea> menuPoints, int radius) {
        mShowAnimation.clear();
        mHideAnimation.clear();

        final int mNumberOfMenu = menuPoints.size();
        final float centerIndex = 1 + (mNumberOfMenu - 1) / 2f;
        int mDistanceToFab = mMenuRenderer.getButtonDistance();
        int margin = mMenuRenderer.getButtonMargin();
        for (int i = 0; i < mNumberOfMenu; i++) {
            final float dir = (centerIndex - (float)(i + 1));
            float side = (dir <= 0f)?-1:1;
            final float finalRadius = side * (Math.abs(dir) * (float)(2 * radius + margin) + mDistanceToFab);
            ObjectAnimator animShow = ObjectAnimator.ofFloat(menuPoints.get(i), "Parameter", 0f, finalRadius);
            animShow.setDuration(animationDuration);
            animShow.setInterpolator(new AnticipateOvershootInterpolator());
            animShow.setStartDelay(0);
            animShow.addUpdateListener(mUpdateListener);
            animShow.addListener(mMenuRenderer.getOnShowListener());
            mShowAnimation.add(animShow);

            ObjectAnimator animHide = animShow.clone();
            animHide.setFloatValues(finalRadius, 0f);
            animHide.setStartDelay(0);
            animHide.removeAllListeners();
            animShow.addUpdateListener(mUpdateListener);
            animHide.addListener(mMenuRenderer.getOnHideListener());
            mHideAnimation.add(animHide);
        }
    }
}
