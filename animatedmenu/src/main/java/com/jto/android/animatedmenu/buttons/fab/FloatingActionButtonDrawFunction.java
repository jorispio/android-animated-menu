package com.jto.android.animatedmenu.buttons.fab;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import androidx.annotation.ColorInt;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.jto.android.animatedmenu.R;

public class FloatingActionButtonDrawFunction implements OnAnimationParameterChange {
    private static int DEFAULT_FAB_RADIUS = 48;
    private boolean initialised;
    private int mCenterX;
    private int mCenterY;
    private Paint mCirclePaint;
    private Bitmap mPlusBitmap;
    private float rotationAngleParameter;
    private Paint mCircleBorder;
    public float mFabButtonRadius;
    private int mMainColor;

    private FloatingActionButtonAnimator animator;

    private Float radiusParameter = 1.0f;

    public FloatingActionButtonDrawFunction(@NonNull Context context) {
        super();
        mCirclePaint = new Paint();
        mMainColor = context.getResources().getColor(R.color.default_color_dark);
        mFabButtonRadius = DEFAULT_FAB_RADIUS;
        animator = new FloatingActionButtonAnimator();
        animator.setListener(this);
    }

    public FloatingActionButtonDrawFunction setRadius(float radius) {
        this.mFabButtonRadius = radius;
        return this;
    }

    public FloatingActionButtonDrawFunction setColor(@ColorInt int color) {
        this.mMainColor = color;
        mCirclePaint.setColor(mMainColor);
        return this;
    }

    public void init(@NonNull Context context, @NonNull View view, @Nullable AttributeSet attrs, @NonNull Resources resources) {

        if (attrs != null) {
            TypedArray a = context.getTheme().obtainStyledAttributes(
                    attrs,
                    R.styleable.AnimatedMenu,
                    0, 0);

            setRadius(a.getDimension(R.styleable.AnimatedMenu_fab_radius, DEFAULT_FAB_RADIUS));
            setColor(a.getColor(R.styleable.AnimatedMenu_fab_background, resources.getColor(R.color.default_color_dark)));

            a.recycle();
        }

        mCirclePaint.setColor(mMainColor);
        mCirclePaint.setStyle(Paint.Style.FILL_AND_STROKE);

        mCircleBorder = new Paint(mCirclePaint);
        mCircleBorder.setStyle(Paint.Style.STROKE);
        mCircleBorder.setStrokeWidth(1f);
        mCircleBorder.setColor(resources.getColor(R.color.default_color_dark));

        mPlusBitmap = BitmapFactory.decodeResource(resources, R.drawable.plus);

        animator.init(view);

        initialised = true;
    }

    public void draw(@NonNull Canvas canvas) {
        if (!initialised) return;

        // central button
        canvas.save();
        canvas.translate(mCenterX, mCenterY);
        Path path = createCentralButtonPath();
        canvas.drawPath(path, mCirclePaint);
        canvas.drawPath(path, mCircleBorder);
        canvas.rotate(rotationAngleParameter);
        canvas.drawBitmap(mPlusBitmap, -mPlusBitmap.getWidth() / 2, -mPlusBitmap.getHeight() / 2, mCirclePaint);
        canvas.restore();
    }

    public void setPosition(int mCenterX, int mCenterY) {
        this.mCenterX = mCenterX;
        this.mCenterY = mCenterY;
    }

    public void onSizeChanged(int w, int h, int oldw, int oldh) {
        mCenterX = w / 2;
        mCenterY = h - (int)mFabButtonRadius;
    }

    public void detach() {
        mPlusBitmap = null;
        animator.clear();
    }

    public int onTouch(MotionEvent event) {
        return -1;
    }

    public boolean isMenuTouch(MotionEvent event) {
        if (event.getX() >= mCenterX - mFabButtonRadius && event.getX() <= mCenterX + mFabButtonRadius) {
            if (event.getY() >= mCenterY - mFabButtonRadius && event.getY() <= mCenterY + mFabButtonRadius) {
                return true;
            }
        }
        return false;
    }

    private Path createCentralButtonPath() {
        Path path = new Path();
        path.moveTo(0, mFabButtonRadius);
        path.addCircle(0, 0, radiusParameter * mFabButtonRadius, Path.Direction.CCW);
        return path;
    }

    @Override
    public void radiusParameterUpdateListener(float value) {
        radiusParameter = value;
    }

    @Override
    public void rotationParameterUpdateListener(float value) {
        rotationAngleParameter = value;
    }

    public void startHideAnimate() {
        animator.getBezierAnimation().start();
        animator.cancelAllAnimation();
        animator.startHideAnimate();
    }

    public void startShowAnimate() {
        animator.startShowAnimate();
    }
}
