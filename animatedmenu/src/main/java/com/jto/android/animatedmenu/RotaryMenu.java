package com.jto.android.animatedmenu;

import android.content.Context;
import android.util.AttributeSet;

import com.jto.android.animatedmenu.renderers.RotaryMenuDrawFunction;

public class RotaryMenu extends AnimatedMenu {
    public RotaryMenu(Context context) {
        this(context, null);
    }

    public RotaryMenu(Context context, AttributeSet attrs) {
        this(context, attrs, 0, 0);
    }

    public RotaryMenu(Context context, AttributeSet attrs, int defStyleAttr) {
        this(context, attrs, defStyleAttr, 0);
    }

    public RotaryMenu(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes, new RotaryMenuDrawFunction(), true);
    }
}
