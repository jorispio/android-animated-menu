package com.jto.android.animatedmenu;

import android.content.Context;
import android.util.AttributeSet;

import com.jto.android.animatedmenu.renderers.RudderMenuDrawFunction;

public class RudderMenu extends AnimatedMenu {
    public RudderMenu(Context context) {
        this(context, null);
    }

    public RudderMenu(Context context, AttributeSet attrs) {
        this(context, attrs, 0, 0);
    }

    public RudderMenu(Context context, AttributeSet attrs, int defStyleAttr) {
        this(context, attrs, defStyleAttr, 0);
    }

    public RudderMenu(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes, new RudderMenuDrawFunction(), true);
    }
}
