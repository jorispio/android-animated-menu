package com.jto.android.animatedmenu;

import android.content.Context;
import android.util.AttributeSet;

import com.jto.android.animatedmenu.renderers.LinearExplodeMenuDrawFunction;

public class LinearMenu extends AnimatedMenu {
    public LinearMenu(Context context) {
        this(context, null);
    }

    public LinearMenu(Context context, AttributeSet attrs) {
        this(context, attrs, 0, 0);
    }

    public LinearMenu(Context context, AttributeSet attrs, int defStyleAttr) {
        this(context, attrs, defStyleAttr, 0);
    }

    public LinearMenu(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes, new LinearExplodeMenuDrawFunction(), false);
    }
}
