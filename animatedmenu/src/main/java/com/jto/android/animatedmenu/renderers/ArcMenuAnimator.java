package com.jto.android.animatedmenu.renderers;

import android.animation.ObjectAnimator;
import android.view.animation.LinearInterpolator;

import com.jto.android.animatedmenu.buttons.MenuButtonArea;

import java.util.ArrayList;

public class ArcMenuAnimator extends AbstractShowOnlyAnimator {
    private static final long DEFAULT_ANIMATION_DURATION = 1000;

    public ArcMenuAnimator(AbstractMenuDrawFunction renderer) {
        super(renderer);
        setAnimationDuration(DEFAULT_ANIMATION_DURATION);
    }

    @Override
    public void updateAnimations(ArrayList<MenuButtonArea> menuPoints, int radius) {
        mShowAnimation.clear();

        int mNumberOfMenu = menuPoints.size();
        for (int i = 0; i < mNumberOfMenu; i++) {
            ObjectAnimator animShow = ObjectAnimator.ofFloat(menuPoints.get(i), "Parameter", 0f, 1f);
            animShow.setDuration(animationDuration);
            animShow.setInterpolator(new LinearInterpolator());
            animShow.setStartDelay(0);
            animShow.addUpdateListener(mUpdateListener);
            animShow.addListener(mMenuRenderer.getOnShowListener());
            mShowAnimation.add(animShow);
        }
    }
}
