package com.jto.android.animatedmenu;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.jto.android.animatedmenu.menu.AnimatedMenuInflater;
import com.jto.android.animatedmenu.menu.AnimatedMenuItem;
import com.jto.android.animatedmenu.renderers.AbstractMenuDrawFunction;
import com.jto.android.animatedmenu.renderers.StarExplodeMenuDrawFunction;

import java.util.ArrayList;

public class AnimatedMenu extends View {
    public static final int[] STATE_ACTIVE =
            {android.R.attr.state_enabled, android.R.attr.state_active};
    public static final int[] STATE_PRESSED =
            {android.R.attr.state_enabled, -android.R.attr.state_active,
                    android.R.attr.state_pressed};
    private static final int MIN_HEIGHT = 160;

    private ArrayList<MenuItem> mMenuItems;
    private Context mContext;

    private int mMinHeight;

    private IMenuExplode mMenuInterface;
    @NonNull private AbstractMenuDrawFunction drawFunction;

    public AnimatedMenu(@NonNull Context context) {
        this(context, null);
    }

    public AnimatedMenu(@NonNull Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public AnimatedMenu(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        this(context, attrs, defStyleAttr, 0);
    }

    public AnimatedMenu(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        this(context, attrs, defStyleAttr, 0, new StarExplodeMenuDrawFunction(), true);
    }

    /** {@Inherit} */
    public AnimatedMenu(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes,
                        @NonNull AbstractMenuDrawFunction renderer, boolean withFloatingActionButton) {
        super(context, attrs, defStyleAttr, defStyleRes);

        this.drawFunction = renderer.withFloatingActionButton(withFloatingActionButton);
        this.mMenuItems = new ArrayList<>();

        this.withFloactingActionButton(withFloatingActionButton);

        init(context, attrs);
    }

    private void init(@NonNull Context context, @Nullable AttributeSet attrs) {
        this.mContext = context;

        if (attrs != null) {
            TypedArray a = context.getTheme().obtainStyledAttributes(
                    attrs,
                    R.styleable.AnimatedMenu,
                    0, 0);

            mMinHeight = (int) a.getDimension(R.styleable.AnimatedMenu_min_height, MIN_HEIGHT);

            if (a.hasValue(R.styleable.AnimatedMenu_menu_items)) {
                int mMenuItemXml = a.getResourceId(R.styleable.AnimatedMenu_menu_items, 0);
                new AnimatedMenuInflater(context).inflate(mMenuItemXml, this);
            }

            a.recycle();
        }

        drawFunction.setPadding(getPaddingTop(), getPaddingBottom(), getPaddingStart(), getPaddingEnd())
                .init(context, this, attrs, context.getResources());
    }

    /** Build the menu.
     * You will need to call invalidate() to update the drawing. */
    public AnimatedMenu build() {
        drawFunction.init(mContext, this);
        drawFunction.onSizeChanged(getWidth(), getHeight(), 0, 0);
        return this;
    }

    /** Set the menu renderer */
    public AnimatedMenu setRenderer(@NonNull AbstractMenuDrawFunction drawFunction) {
        this.drawFunction = drawFunction;
        return this;
    }

    /** Get the current menu renderer */
    public AbstractMenuDrawFunction getRenderer() {
        return drawFunction;
    }

    /** Set floating action button for the menu.
     * In general, this is already set by the menu template. */
    public AnimatedMenu withFloactingActionButton(boolean withFloatingActionButton) {
        drawFunction.withFloatingActionButton(withFloatingActionButton);
        return this;
    }

    /** Get the menu inflater to populate the list of menu items.
     * A menu xml declaration shall be used.s */
    public AnimatedMenuInflater getMenuInflater() {
        return new AnimatedMenuInflater(mContext);
    }

    /** Remove all menu items */
    public AnimatedMenu clearMenuItems() {
        mMenuItems.clear();
        return this;
    }

    /** Add one single menu items.
     * It is preferable to build the menu through the XML menu inflater. */
    public AnimatedMenuItem addMenuItem(int groupId, int itemId, int itemCategoryOrder, CharSequence itemTitle) {
        AnimatedMenuItem mMenuItem = new AnimatedMenuItem(mContext, groupId, itemId, 0, itemCategoryOrder, itemTitle);
        mMenuItems.add(itemCategoryOrder, mMenuItem);
        return mMenuItem;
    }

    public void completeMenu() {
        drawFunction.addMenuItems(mMenuItems);
    }

    /** Set menu listener. */
    public AnimatedMenu setOnMenuListener(IMenuExplode onMenuListener) {
        mMenuInterface = onMenuListener;
        return this;
    }

    private int measureHeight(int measureSpec) {
        int size = getPaddingTop() + getPaddingBottom();
        size += drawFunction.getDesiredHeight();
        size = Math.max(size, mMinHeight);
        return resolveSizeAndState(size, measureSpec, 0);
    }

    private int measureWidth(int measureSpec) {
        //int size = getPaddingLeft() + getPaddingRight();
        //size += mXdpi;
        //float MARGIN = 10f;
        //size += 2 * MARGIN;
        int size = getMeasuredWidth();
        return resolveSizeAndState(size, measureSpec, 0);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        setMeasuredDimension(measureWidth(widthMeasureSpec), measureHeight(heightMeasureSpec));
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        drawFunction.onSizeChanged(w, h, oldw, oldh);
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        drawFunction.detach();
    }

    /** */
    public void onResume() {
        drawFunction.onResume();
        invalidate();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        drawFunction.draw(canvas);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                if (drawFunction.isMenuTouch(event)) {
                    return true;
                }
                int menuItem = isMenuItemTouched(event);
                if (drawFunction.isMenuVisible() && menuItem > 0) {
                    if (drawFunction.setState(menuItem, STATE_PRESSED)) {
                        invalidate();
                    }

                    return true;
                }
                return false;
            case MotionEvent.ACTION_UP:
                if (drawFunction.isMenuTouch(event)) {
                    if (drawFunction.isMenuVisible()) {
                        drawFunction.startHideAnimate();
                        if (mMenuInterface != null) {
                            mMenuInterface.onMenuClose();
                        }
                    } else {
                        drawFunction.startShowAnimate();
                        if (mMenuInterface != null) {
                            mMenuInterface.onMenuOpen();
                        }
                    }
                    return true;
                }

                if (drawFunction.isMenuVisible()) {
                    menuItem = isMenuItemTouched(event);
                    invalidate();
                    if (menuItem > 0) {
                        if (drawFunction.setState(menuItem, STATE_ACTIVE)) {
                            postInvalidateDelayed(1000);
                        }
                        if (mMenuInterface != null) {
                            mMenuInterface.onMenuItemClicked(menuItem);
                        }
                        return true;
                    }
                }
                return false;

        }
        return true;
    }

    private int isMenuItemTouched(MotionEvent event) {
        if (!drawFunction.isMenuVisible()) {
            return -1;
        }

        return drawFunction.onTouch(event);
    }

    /** Close the menu */
    public void close() {
        drawFunction.startHideAnimate();
    }

    /** Open the menu */
    public void open() {
        drawFunction.startShowAnimate();
    }
}
