package com.jto.android.animatedmenu.renderers;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.jto.android.animatedmenu.R;
import com.jto.android.animatedmenu.buttons.ButtonRenderer;
import com.jto.android.animatedmenu.buttons.MenuButtonArea;

public class GenericMenuDrawFunction extends AbstractMenuDrawFunction<GenericMenuDrawFunction> {
    private static float DEG2RAD = (float)Math.PI / 180f;

    private float mArcRadius;
    private float mArcEccentricity;
    private double mAngularRange;
    private double mAngularRangeMin;
    private double mAngularRangeMax;
    private int nbRows;
    private int[] btnPerRows;
    private boolean mRearrangeButtons;

    public GenericMenuDrawFunction() {
        super(true);
        animator = new StarExplodeMenuAnimator(this);
        mArcRadius = mDistanceToFab;
        mArcEccentricity = 0;
        mAngularRangeMin = 0;
        mAngularRangeMax = Math.PI;
        mAngularRange = Math.PI;
        nbRows = 1;
        mRearrangeButtons = false;
    }

    /** Set arc radius, taking the fictional Fab position as reference
     *
     * @param arcRadius arc radius from fab center, px
     * @return this
     */
    public GenericMenuDrawFunction setArcRadius(float arcRadius) {
        this.mArcRadius = arcRadius;
        return this;
    }

    /** Return the arc radius */
    public double getArcRadius() {
        return mArcRadius;
    }

    /** Set the eccentricity of the arc
     * @param eccentricity eccentricity value between 0f and 1f
     */
    public GenericMenuDrawFunction setArcEccentricity(float eccentricity) {
        this.mArcEccentricity = eccentricity;
        return this;
    }

    /** Get the eccentricity of the arc */
    public double getArcEccentricity() {
        return mArcEccentricity;
    }

    /** Set arc extension range.
     * Note there will be a button on both ends. If the angular range is exactly 2 PI though, the
     * buttons will be spread on [0 2Pi[ with no overlap.
     *
     * @param angularRangeMin arc angle start, radians
     * @param angularRangeMax arc angle stop, radians
     * @return this
     */
    public GenericMenuDrawFunction setAngularRange(double angularRangeMin, double angularRangeMax) {
        this.mAngularRangeMin = angularRangeMin;
        this.mAngularRangeMax = angularRangeMax;
        this.mAngularRange = mAngularRangeMax - mAngularRangeMin;
        return this;
    }

    /** Return the arc start angle in degrees */
    public double getAngularRangeStart() {
        return mAngularRangeMin / DEG2RAD;
    }

    /** Return the arc sweep angle in degrees */
    public double getAngularRangeSweep() {
        return mAngularRange / DEG2RAD;
    }

    public GenericMenuDrawFunction setRows(int nbRows) {
        this.nbRows = nbRows;
        return this;
    }

    /** Return the computed number of rows to fit all the buttons */
    public int getRows() {
        return nbRows;
    }

    @Override
    public void init(@NonNull Context context, @NonNull View view, @Nullable AttributeSet attrs, Resources resources) {
        super.init(context, view, attrs, resources);

        if (attrs != null) {
            TypedArray a = context.getTheme().obtainStyledAttributes(
                    attrs,
                    R.styleable.AnimatedMenu,
                    0, 0);

            mRearrangeButtons = a.getBoolean(R.styleable.AnimatedMenu_menu_button_rearrange, true);

            nbRows = a.getInt(R.styleable.AnimatedMenu_menu_buttons_rows, 1);

            if (a.hasValue(R.styleable.AnimatedMenu_arc_eccentricity)) {
                mArcEccentricity = a.getFloat(R.styleable.AnimatedMenu_arc_eccentricity, 0f);
            }

            mArcRadius = (int) a.getDimension(R.styleable.AnimatedMenu_arc_radius, mDistanceToFab);

            if (a.hasValue(R.styleable.AnimatedMenu_arc_angle_start)) {
                mAngularRangeMin = a.getFloat(R.styleable.AnimatedMenu_arc_angle_start, 0) * DEG2RAD;
            }
            if (a.hasValue(R.styleable.AnimatedMenu_arc_angle_stop)) {
                mAngularRangeMax = a.getFloat(R.styleable.AnimatedMenu_arc_angle_stop, 180) * DEG2RAD;
            }
            setAngularRange(mAngularRangeMin, mAngularRangeMax);

            a.recycle();
        }

        init(context, view);
    }

    @Override
    public void init(@NonNull Context context, @NonNull View view) {
        super.init(context, view);
        animator.init(view);

        mButtonRenderer = new ButtonRenderer(context);
        ((ButtonRenderer) mButtonRenderer).setColor(mButtonsColor)
                .setBackground(mMenuButtonBackground)
                .setRadius(mMenuButtonRadius)
                .withLabels(mWithLabels);

        // compute the number of buttons on each circular row
        computeNumberOfButtons();
    }

    private int computeMaximumNumberOfButtonsPerRow(float radius, float deltaAngle) {
        float length = radius * deltaAngle;
        return (int) Math.floor(length / (2 * mMenuButtonRadius + mMenuButtonsMargin));
    }

    private void computeNumberOfButtons() {
        btnPerRows = new int[mMenuItems.size()];
        if (!mMenuItems.isEmpty()) {
            btnPerRows[0] = mMenuItems.size();

            if (mRearrangeButtons) {
                float radius = getButtonDistance();
                int nbBtns = 0;
                nbRows = 0;

                int nbOfButtons = mMenuItems.size();
                while (nbBtns < nbOfButtons) {
                    btnPerRows[nbRows] = computeMaximumNumberOfButtonsPerRow(radius, (float)mAngularRange);
                    btnPerRows[nbRows] = Math.min(btnPerRows[nbRows], nbOfButtons - nbBtns);
                    nbBtns += btnPerRows[nbRows];
                    radius += 2 * mMenuButtonRadius + mMenuButtonsMargin;
                    ++nbRows;
                }
            }
        }
    }

    @Override
    public int getDesiredHeight() {
        return nbRows * mDistanceToFab;
    }

    /** Compute the current button position from the animated parameter */
    protected  Point getButtonPosition(MenuButtonArea menuButtonArea) {
        float radius = menuButtonArea.getParameter();
        float x = (float) (radius * Math.cos(menuButtonArea.getAngle()));
        float y = (float) (radius * Math.sin(menuButtonArea.getAngle()));
        return new Point((int)x, (int)y);
    }

    private double getAngularSpacing(int iCurrentRow) {
        if (mAngularRange / DEG2RAD < 360) {
            return (mAngularRange / (float) (btnPerRows[iCurrentRow] - 1));
        }
        return (mAngularRange / (float) (btnPerRows[iCurrentRow]));
    }

    protected float getArcRadius(double angle) {
        return mArcRadius / ( 1f + mArcEccentricity * (float) Math.sin(angle));
    }

    public float getRowRadius(int iRow) {
        return (float) getArcRadius(0) + ((float) iRow) * (2 * mMenuButtonRadius + mMenuButtonsMargin);
    }

    public float getRowCurrentRadius(int iRow) {
        int nbButtons = 0;
        for (int i = 0; i < iRow; i++) {
            nbButtons += btnPerRows[i];
        }

        final MenuButtonArea menuButtonArea = mMenuPoints.get(nbButtons);
        return menuButtonArea.getParameter();
    }

    @Override
    public void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);

        computeNumberOfButtons();

        int nbButtons = 0;
        int iCurrentRow = 0;
        for (int i = 0; i < getNumberOfMenu(); i++) {
            if (btnPerRows[iCurrentRow] <= i - nbButtons) {
                nbButtons = i;
                ++iCurrentRow;
            }

            final float angle = (float) (mAngularRangeMin + getAngularSpacing(iCurrentRow) * (i - nbButtons));
            final float rowRadius = mArcRadius + ((float) iCurrentRow) * (2 * mMenuButtonRadius + mMenuButtonsMargin);

            MenuButtonArea menuButtonArea = new MenuButtonArea();
            menuButtonArea.setRadius(rowRadius)
                          .setParameter(rowRadius);
            menuButtonArea.setAngle(angle);
            mMenuPoints.add(menuButtonArea);
        }

        if (mDrawableArray != null) {
            for (Object drawable : getDrawableArray()) {
                if (drawable != null) {
                    ((Drawable) drawable).setBounds(0, 0, mMenuButtonRadius, mMenuButtonRadius);
                }
            }
        }

        animator.updateAnimations(getMenuPoints(), getButtonDistance());
        onResume();
    }

    @Override
    public int onTouch(MotionEvent event) {
        for (int i = 0; i < mMenuPoints.size(); i++) {
            MenuButtonArea menuButtonArea = mMenuPoints.get(i);
            Point relativePoint = getButtonPosition(menuButtonArea);
            float x = relativePoint.x + getCenterX();
            float y = getCenterY() - relativePoint.y;
            if (menuButtonArea.isClicked(x, y, event, mMenuButtonRadius)) {
                    return getMenuItems().get(i).getItemId();
            }
        }
        return -1;
    }
}
