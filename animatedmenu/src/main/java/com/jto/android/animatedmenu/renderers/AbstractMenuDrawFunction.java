package com.jto.android.animatedmenu.renderers;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;

import androidx.annotation.CallSuper;
import androidx.annotation.ColorInt;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.jto.android.animatedmenu.R;
import com.jto.android.animatedmenu.buttons.ButtonRenderer;
import com.jto.android.animatedmenu.buttons.IButtonRenderer;
import com.jto.android.animatedmenu.buttons.MenuButtonArea;
import com.jto.android.animatedmenu.buttons.fab.FloatingActionButtonDrawFunction;

import java.util.ArrayList;
import java.util.List;

public abstract class AbstractMenuDrawFunction<T extends AbstractMenuDrawFunction> {
    private static int DEFAULT_DISTANCE = 80;
    private static int DEFAULT_FAB_RADIUS = 48;
    private static int DEFAULT_MENU_BUTTON_RADIUS = 24;
    private static int DEFAULT_MENU_BUTTON_MARGIN = 12;
    private static boolean DEFAULT_WITH_LABELS = true;
    private static boolean DEFAULT_WITH_FAB = true;
    private static float DEFAULT_LABELS_TEXT_SIZE = 20;
    private static final long DEFAULT_ANIMATION_DURATION = 1000;

    private View view;
    private Context mContext;

    private FloatingActionButtonDrawFunction fab;
    private boolean mWithFloatingActionButton;
    int mFloatingActionButtonRadius;
    private int mFloatingActionButtonColor;

    int mMenuButtonRadius;
    int mDistanceToFab;
    int mMenuButtonsMargin;

    boolean isMenuVisible;
    boolean mWithLabels;
    @ColorInt int mButtonsColor;
    @ColorInt int mButtonsDarkColor;
    Drawable mMenuButtonBackground;
    IButtonRenderer mButtonRenderer;
    private float mButtonLabelTextSize;

    private int mAnimationDuration;

    private int mPaddingTop;
    private int mPaddingBottom;
    private int mPaddingStart;
    private int mPaddingEnd;
    private int mLayoutGravity;

    ArrayList<MenuButtonArea> mMenuPoints;
    List<Drawable> mDrawableArray;
    List<MenuItem> mMenuItems;
    IMenuAnimator animator;

    private int mHeight;
    private int mCenterX;
    private int mCenterY;
    protected boolean initialised;

    AbstractMenuDrawFunction() {
        this(DEFAULT_WITH_FAB);
    }

    AbstractMenuDrawFunction(boolean isWithFloatingActionButton) {
        this.mMenuItems = new ArrayList<>();
        this.mMenuPoints = new ArrayList<>();
        this.mDrawableArray = new ArrayList<>();

        this.mWithFloatingActionButton = isWithFloatingActionButton;
        this.mFloatingActionButtonRadius = DEFAULT_FAB_RADIUS;
        this.mFloatingActionButtonColor= 0;

        this.mDistanceToFab = DEFAULT_DISTANCE;

        this.mMenuButtonRadius = DEFAULT_MENU_BUTTON_RADIUS;
        this.mMenuButtonsMargin = DEFAULT_MENU_BUTTON_MARGIN;
        this.mMenuButtonBackground = null;
        this.mWithLabels = DEFAULT_WITH_LABELS;
        this.mButtonLabelTextSize = DEFAULT_LABELS_TEXT_SIZE;

        this.mLayoutGravity = Gravity.NO_GRAVITY;

        this.initialised = false;
    }

    @CallSuper
    public void init(@NonNull Context context, @NonNull View view, @Nullable AttributeSet attrs, Resources resources) {
        this.view = view;
        this.mContext = context;

        if (attrs != null) {
            TypedArray a = context.getTheme().obtainStyledAttributes(
                    attrs,
                    R.styleable.AnimatedMenu,
                    0, 0);

            mDistanceToFab = (int) a.getDimension(R.styleable.AnimatedMenu_gap_between_menu_fab, DEFAULT_DISTANCE);

            mMenuButtonRadius = (int) a.getDimension(R.styleable.AnimatedMenu_menu_button_radius, DEFAULT_MENU_BUTTON_RADIUS);
            mMenuButtonsMargin = (int) a.getDimension(R.styleable.AnimatedMenu_menu_button_margin, DEFAULT_MENU_BUTTON_MARGIN);

            mButtonsColor = a.getColor(R.styleable.AnimatedMenu_menu_color, resources.getColor(R.color.default_color));
            mButtonsDarkColor = a.getColor(R.styleable.AnimatedMenu_menu_darkcolor, resources.getColor(R.color.default_color_dark));

            // we can overwrite the renderer choice
            if (a.hasValue(R.styleable.AnimatedMenu_with_labels)) {
                mWithLabels = a.getBoolean(R.styleable.AnimatedMenu_with_labels, DEFAULT_WITH_LABELS);
            }

            mButtonLabelTextSize = a.getDimension(R.styleable.AnimatedMenu_button_label_text_size, DEFAULT_LABELS_TEXT_SIZE);

            if (a.hasValue(R.styleable.AnimatedMenu_menu_button_background)) {
                mMenuButtonBackground = a.getDrawable(R.styleable.AnimatedMenu_menu_button_background);
            }

            mFloatingActionButtonRadius = (int) a.getDimension(R.styleable.AnimatedMenu_fab_radius, DEFAULT_FAB_RADIUS);
            mFloatingActionButtonColor = a.getColor(R.styleable.AnimatedMenu_fab_background, resources.getColor(R.color.default_color_dark));

            // we can overwrite the renderer choice
            if (a.hasValue(R.styleable.AnimatedMenu_with_fab)) {
                mWithFloatingActionButton = a.getBoolean(R.styleable.AnimatedMenu_with_fab, DEFAULT_WITH_FAB);
            }

            mAnimationDuration = a.getInt(R.styleable.AnimatedMenu_animation_duration, (int)DEFAULT_ANIMATION_DURATION);

            mLayoutGravity = a.getInt(R.styleable.AnimatedMenu_android_layout_gravity, Gravity.NO_GRAVITY);

            a.recycle();
        }

        if (mWithFloatingActionButton) {
            fab = new FloatingActionButtonDrawFunction(context);
            fab.init(context, view, attrs, resources);
        }

        mButtonRenderer = new ButtonRenderer(context);
        ((ButtonRenderer) mButtonRenderer).setColor(mButtonsColor)
                .setBorderColor(mButtonsDarkColor)
                .setRadius(mMenuButtonRadius)
                .setBackground(mMenuButtonBackground)
                .setLabelTextSize(mButtonLabelTextSize)
                .withLabels(mWithLabels);

        animator.setAnimationDuration(mAnimationDuration);

        initialised = true;
    }

    @CallSuper
    public void init(@NonNull Context context, @NonNull View view) {
        this.view = view;
        this.mContext = context;

        if (mWithFloatingActionButton) {
            fab = getFloatingActionButton();
            fab.setRadius(mFloatingActionButtonRadius)
               .setColor(mFloatingActionButtonColor);
            fab.init(context, view, null, context.getResources());
            fab.setPosition(getCenterX(), getCenterY());
        }
        initialised = true;
    }

    /** Floating action button visibility status
     *
     * @param enable if true, show a floating action button.
     * @return this
     */
    public final AbstractMenuDrawFunction<T> withFloatingActionButton(boolean enable) {
        this.mWithFloatingActionButton = enable;
        if (!mWithFloatingActionButton) {
            fab = null;
        }
        return this;
    }

    @Nullable
    public FloatingActionButtonDrawFunction getFloatingActionButton() {
        if (mWithFloatingActionButton && (fab == null) && (mContext != null)) {
            fab = new FloatingActionButtonDrawFunction(mContext);
        }
        return fab;
    }

    /** Get Floating action button visibility status */
    public boolean isWithFloatingActionButton() {
        return mWithFloatingActionButton;
    }

    public final AbstractMenuDrawFunction<T> withLabels(boolean enable) {
        this.mWithLabels = enable;
        return this;
    }

    /** Set menu button drawable.
     * @param drawable drawable for menu buttons
     */
    public final AbstractMenuDrawFunction<T> setMenuButtonBackground(Drawable drawable) {
        this.mMenuButtonBackground = drawable;
        return this;
    }

    /** Get menu button drawable. */
    public final Drawable getMenuButtonBackground() {
        return mMenuButtonBackground;
    }

    public final AbstractMenuDrawFunction<T> setColor(@ColorInt int mButtonsColor) {
        this.mButtonsColor = mButtonsColor;
        return this;
    }

    public final int getColor() {
        return mButtonsColor;
    }

    public final AbstractMenuDrawFunction<T> setColorDark(@ColorInt int mButtonsColor) {
        this.mButtonsDarkColor = mButtonsColor;
        return this;
    }

    public final int getColorDark() {
        return mButtonsColor;
    }

    /** Set the distance between the main button and the next menu button.
     * This distance shall be at least the radius of the main button if it is visible.
     * @param  distance distance between main button and the nearest menu button
     * @return this
     * */
    public final AbstractMenuDrawFunction<T> setButtonDistance(int distance) {
        this.mDistanceToFab = distance;
        return this;
    }

    /** Return the button distance to center */
    final int getButtonDistance() {
        return mDistanceToFab;
    }

    /** Set the button margin
     * @param margin margin around the button, px */
    public final AbstractMenuDrawFunction<T> setButtonMargin(int margin) {
        this.mMenuButtonsMargin = margin;
        return this;
    }

    /** Return the button margin */
    final int getButtonMargin() {
        return mMenuButtonsMargin;
    }

    /** Set floating action button radius
     *
     * @param radius radius of floating action button
     * @return this
     */
    public final AbstractMenuDrawFunction<T> setMainMenuButtonRadius(int radius) {
        this.mFloatingActionButtonRadius = radius;
        if (getFloatingActionButton() != null) {
            getFloatingActionButton().setRadius(radius);
        }
        return this;
    }

    /** get Floating Action Button radius */
    public final int getMainMenuButtonRadius() {
        return mFloatingActionButtonRadius;
    }

    /** Set floating action button color
     *
     * @param color background color of floating action button
     * @return this
     */
    public AbstractMenuDrawFunction<T> setMainMenuButtonColor(int color) {
        this.mFloatingActionButtonColor = color;
        if (getFloatingActionButton() != null) {
            getFloatingActionButton().setColor(color);
        }
        return this;
    }

    /** Set menu button radius
     *
     * @param radius menu button radius, px
     * @return this
     */
    public final AbstractMenuDrawFunction<T> setMenuButtonRadius(int radius) {
        this.mMenuButtonRadius = radius;
        return this;
    }

    /** Get menu button radius */
    public int getMenuButtonRadius() {
        return mMenuButtonRadius;
    }

    /** Set animator
     *
     * @param animator animator
     * @return this
     */
    AbstractMenuDrawFunction<T> setAnimator(@NonNull IMenuAnimator animator) {
        this.animator = animator;
        return this;
    }

    /** Get animator */
    IMenuAnimator getAnimator() {
        return animator;
    }

    /** Set menu animation duration
     *
     * @param duration animation duration in ms
     * @return this
     */
    public AbstractMenuDrawFunction<T> setAnimationDuration(int duration) {
        this.mAnimationDuration = duration;
        return this;
    }

    /** return the animation duration in ms */
    public int getAnimationDuration() {
        return mAnimationDuration;
    }

    /** Set renderer for the menu buttons.
     * The renderer shall implement IButtonRenderer, and allow updating button drawable and labels.
     * If labels are not used or do not need to be modified, it is advised to use setButtonDrawable()
     * instead.
     *
     * @param mButtonBitmap menu button bitmap rendering function
     */
    public final AbstractMenuDrawFunction<T> setButtonRenderer(IButtonRenderer mButtonBitmap) {
        this.mButtonRenderer = mButtonBitmap
                .setBackground(mMenuButtonBackground)
                .setRadius(mMenuButtonRadius)
                .setLabelTextSize(mButtonLabelTextSize)
                .withLabels(mWithLabels);

        return this;
    }

    /** Get button renderer */
    public final IButtonRenderer getButtonRenderer() {
        return mButtonRenderer;
    }


    /** Set drawable for the menu buttons
     * @param drawable drawable for the menu buttons
     */
    public final AbstractMenuDrawFunction<T> setButtonDrawable(Drawable drawable) {
        this.mButtonRenderer.setBackground(drawable);
        return this;
    }

    /** Get menu button drawable */
    protected final Drawable getButtonDrawable() {
        return mButtonRenderer.getDrawable();
    }

    public AbstractMenuDrawFunction<T> setPadding(int paddingTop, int paddingBottom, int paddingStart, int paddingEnd) {
        mPaddingTop = paddingTop;
        mPaddingBottom = paddingBottom;
        mPaddingStart = paddingStart;
        mPaddingEnd = paddingEnd;
        return this;
    }

    public int getPaddingTop() {
        return mPaddingTop;
    }

    public int getPaddingBottom() {
        return mPaddingBottom;
    }

    public int getPaddingStart() {
        return mPaddingStart;
    }

    public int getPaddingEnd() {
        return mPaddingEnd;
    }

    public AbstractMenuDrawFunction<T> setLayoutGravity(int gravity) {
        this.mLayoutGravity = gravity;
        return this;
    }

    public int getLayoutGravity() {
        return mLayoutGravity;
    }

    public int getHeight() {
        return mHeight;
    }

    public int getDesiredHeight() {
        return mHeight;
    }

    /** Return the center X-position of the FAB */
    int getCenterX() {
        return mCenterX;
    }

    /** Return the center X-position of the FAB taking into account gravity choice,
     * and margins.
     * */
    public int getCenterX(int w) {
        int hGravity = mLayoutGravity & Gravity.HORIZONTAL_GRAVITY_MASK;
        if ((hGravity == Gravity.START) || (hGravity == Gravity.LEFT)) {
            return getPaddingStart() + mFloatingActionButtonRadius;
        } else if (hGravity == Gravity.CENTER_HORIZONTAL) {
            return w / 2;
        } else if ((hGravity == Gravity.END) || (hGravity == Gravity.RIGHT)) {
            return w - mFloatingActionButtonRadius - getPaddingEnd();
        }
        return w / 2;
    }

    /** Return the center Y-position of the FAB */
    int getCenterY() {
        return mCenterY;
    }

    /** Return the center Y-position of the FAB taking into account gravity choice
     *  */
    public int getCenterY(int h) {
        int vGravity = mLayoutGravity & Gravity.VERTICAL_GRAVITY_MASK;
        if (vGravity == Gravity.CENTER_VERTICAL) {
            return h / 2;
        } else if (vGravity == Gravity.BOTTOM) {
            return h - mFloatingActionButtonRadius - getPaddingBottom();
        } else if (vGravity == Gravity.TOP) {
            return mFloatingActionButtonRadius + getPaddingTop();
        }
        return mFloatingActionButtonRadius + getPaddingTop();
    }

    protected abstract Point getButtonPosition(MenuButtonArea menuButtonArea);

    @CallSuper
    public void draw(Canvas canvas) {
        if (!initialised) return;

        for (int i = 0; i < mMenuPoints.size(); i++) {
            final MenuButtonArea menuButtonArea = mMenuPoints.get(i);
            Point pt = getButtonPosition(menuButtonArea);
            mButtonRenderer.draw(canvas, pt.x + getCenterX(), getCenterY() - pt.y, mMenuItems.get(i).getTitle().toString());

            if (i < mDrawableArray.size()) {
                canvas.save();
                canvas.translate(pt.x + getCenterX() - mMenuButtonRadius / 2f, getCenterY() - pt.y - mMenuButtonRadius / 2f);
                getDrawableArray().get(i).draw(canvas);
                canvas.restore();
            }
        }

        if (fab != null) {
            fab.draw(canvas);
        }
    }

    @CallSuper
    public void onSizeChanged(int w, int h, int oldw, int oldh) {
        mMenuPoints.clear();

        if (fab != null) {
            fab.onSizeChanged(w, h, oldw, oldh);
        }

        mHeight = h;
        mCenterX = getCenterX(w);
        mCenterY = getCenterY(h);
        if (fab != null) {
            fab.setPosition(getCenterX(), getCenterY());
        }
    }

    abstract public int onTouch(MotionEvent event);

    public final boolean isMenuTouch(MotionEvent event) {
        if (fab != null) {
            if (event.getX() >= mCenterX - fab.mFabButtonRadius && event.getX() <= mCenterX + fab.mFabButtonRadius) {
                return event.getY() >= mCenterY - fab.mFabButtonRadius && event.getY() <= mCenterY + fab.mFabButtonRadius;
            }
        }
        return false;
    }

    @CallSuper
    public final boolean setState(int menuItem, int[] state) {
        if ((menuItem >= 0) && (menuItem <= mDrawableArray.size())) {
            getDrawableArray().get(menuItem).setState(state);
            return true;
        }
        return false;
    }

    final ArrayList<MenuButtonArea> getMenuPoints() {
        return mMenuPoints;
    }

    @CallSuper
    private T addDrawable(Drawable drawable) {
        drawable.setBounds(0, 0, mMenuButtonRadius, mMenuButtonRadius);
        mDrawableArray.add(drawable);
        return (T) this;
    }

    final List<Drawable> getDrawableArray() {
        return mDrawableArray;
    }

    /** Set the menu items */
    @CallSuper
    public final T addMenuItems(@NonNull ArrayList<MenuItem> menuItems) {
        mMenuItems.clear();
        mMenuItems.addAll(menuItems);

        mDrawableArray.clear();
        for (MenuItem item : menuItems) {
            addDrawable(item.getIcon());
        }

        return (T) this;
    }

    /** Get the list of declared menu items.
     * These menu items shall be added with #addMenuItems or through XML attribute */
    public final List<MenuItem> getMenuItems() {
        return mMenuItems;
    }

    public final int getNumberOfMenu() {
        return mMenuItems.size();
    }

    /** Release resources */
    public void detach() {
        if (fab != null) {
            fab.detach();
        }
        animator.clear();
    }

    public void onResume() {
        startShowAnimate();
    }

    @CallSuper
    public void startHideAnimate() {
        if (fab != null) {
            fab.startHideAnimate();
        }
        animator.cancelAllAnimation();
        if (animator.startHideAnimate()) {
            isMenuVisible = false;
        }
    }

    @CallSuper
    public void startShowAnimate() {
        if (fab != null) {
            fab.startShowAnimate();
        }
        if (animator.startShowAnimate()) {
            isMenuVisible = true;
        }
    }

    public boolean isMenuVisible() {
        return isMenuVisible;
    }

    Animator.AnimatorListener getOnShowListener() {
        return new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                if (!initialised) return;
                mButtonRenderer.showLabels();
                view.invalidate();
            }
        };
    }

    AnimatorListenerAdapter getOnHideListener() {
        return new AnimatorListenerAdapter() {

            @Override
            public void onAnimationStart(Animator animation) {
                if (!initialised) return;
                mButtonRenderer.hideLabels();
                view.invalidate();
            }
        };
    }
}
