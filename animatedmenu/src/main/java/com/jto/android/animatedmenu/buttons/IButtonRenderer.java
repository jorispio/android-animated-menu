package com.jto.android.animatedmenu.buttons;

import android.graphics.Canvas;
import android.graphics.drawable.Drawable;

public interface IButtonRenderer {
    IButtonRenderer setRadius(int mMenuButtonRadius);
    IButtonRenderer setBackground(Drawable mMenuButtonBackground);
    Drawable getDrawable();

    IButtonRenderer setLabelTextSize(float mButtonLabelTextSize);
    IButtonRenderer setLabelBackgroundColor(int color);
    IButtonRenderer setLabelTextMargin(int textMargin);
    IButtonRenderer withLabels(boolean withLabels);
    void hideLabels();
    void showLabels();

    void draw(Canvas canvas, float x, float y, String text);
}
