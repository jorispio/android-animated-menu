package com.jto.android.animatedmenu.menu;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.MenuItem;

import androidx.annotation.NonNull;

import com.jto.android.animatedmenu.AnimatedMenu;
import com.jto.android.animatedmenu.R;

class MenuState {
    private Context mContext;
    private AnimatedMenu menu;

    private static final int USER_MASK = 0x0000ffff;
    private static final int NO_ID = 0;
    private static final int defaultGroupId = NO_ID;
    private static final int defaultItemId = NO_ID;
    private static final int defaultItemOrder = 0;

    private int groupId;
    private int groupOrder;

    private boolean itemAdded;
    private int itemId;
    private int itemOrder;
    private CharSequence itemTitle;
    private CharSequence itemTitleCondensed;
    private int itemIconResId;

    private boolean itemVisible;
    private boolean itemEnabled;

    MenuState(Context mContext, AnimatedMenu menu) {
        this.mContext = mContext;
        this.menu = menu;
    }

    /** Read group. Not implemented, group are not handled in the menu.
     */
    public void readGroup(AttributeSet attrs) {
        groupId = defaultGroupId;
        groupOrder = defaultItemOrder;
    }

    /** Read item
     *
     * @param attrs XML attributes
     */
    void readItem(@NonNull final AttributeSet attrs) {
        TypedArray a = mContext.obtainStyledAttributes(attrs, R.styleable.AnimatedMenuItem);

        itemId = a.getResourceId(R.styleable.AnimatedMenuItem_android_id, defaultItemId);
        final int order = a.getInt(R.styleable.AnimatedMenuItem_android_orderInCategory, groupOrder);
        itemOrder = (order & USER_MASK);
        itemTitle = a.getText(R.styleable.AnimatedMenuItem_android_title);
        itemTitleCondensed = a.getText(R.styleable.AnimatedMenuItem_android_titleCondensed);
        itemIconResId = a.getResourceId(R.styleable.AnimatedMenuItem_android_icon, 0);

        itemVisible = a.getBoolean(R.styleable.AnimatedMenuItem_android_visible, true);
        itemEnabled = a.getBoolean(R.styleable.AnimatedMenuItem_android_enabled, true);

        a.recycle();
        itemAdded = false;
    }

    /**
     * Indicate whether item was added.
     * @return true if item was added
     */
    public boolean hasAddedItem() {
        return itemAdded;
    }

    /**
     * Construct and add menu item and return the build item
     * @return this
     */
    public MenuItem addItem() {
        itemAdded = true;
        AnimatedMenuItem item = menu.addMenuItem(groupId, itemId, itemOrder, itemTitle);
        setItem(item);
        return item;
    }

    private void setItem(@NonNull AnimatedMenuItem item) {
        item.setVisible(itemVisible)
                .setEnabled(itemEnabled)
                .setTitle(itemTitle);
        if (itemTitleCondensed != null) {
            item.setTitleCondensed(itemTitleCondensed);
        }
        if (itemIconResId > 0) {
            item.setIcon(itemIconResId);
        } else {
            item.setIcon(R.drawable.default_item_drawable);
        }
    }
}
