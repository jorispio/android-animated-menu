package com.jto.android.animatedmenu.buttons.fab;

public interface OnAnimationParameterChange {
    void radiusParameterUpdateListener(float value);
    void rotationParameterUpdateListener(float value);
}
