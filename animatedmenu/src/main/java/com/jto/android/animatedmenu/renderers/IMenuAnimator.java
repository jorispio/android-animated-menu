package com.jto.android.animatedmenu.renderers;

import android.view.View;

import androidx.annotation.NonNull;

import com.jto.android.animatedmenu.buttons.MenuButtonArea;

import java.util.ArrayList;

public interface IMenuAnimator {
    void init(@NonNull View view);
    void clear();
    void cancelAllAnimation();
    boolean startHideAnimate();
    boolean startShowAnimate();
    IMenuAnimator setAnimationDuration(long mAnimationDuration);
    void updateAnimations(ArrayList<MenuButtonArea> menuPoints, int gab);
}
