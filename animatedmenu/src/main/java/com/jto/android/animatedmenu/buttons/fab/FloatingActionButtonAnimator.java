package com.jto.android.animatedmenu.buttons.fab;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.LinearInterpolator;

import androidx.annotation.NonNull;

import com.jto.android.animatedmenu.buttons.MenuButtonArea;
import com.jto.android.animatedmenu.renderers.IMenuAnimator;

import java.util.ArrayList;

public class FloatingActionButtonAnimator implements IMenuAnimator {
    private static final long DEFAULT_ANIMATION_DURATION = 800;
    private static final float START_ROTATION_ANGLE = 0f;
    private static final float END_ROTATION_ANGLE = 45f;
    private static final float START_ROTATION_RADIUS = 0.8f;
    private static final float END_ROTATION_RADIUS = 1.0f;

    private OnAnimationParameterChange mOnAnimationParameterChange;

    private View view;
    private ValueAnimator mRadiusStartAnimation, mRadiusEndAnimation;
    private ValueAnimator mRotationAnimation;
    private ValueAnimator mRotationReverseAnimation;

    private long animationDuration;

    FloatingActionButtonAnimator() {
        setAnimationDuration(DEFAULT_ANIMATION_DURATION);
    }

    @Override
    public FloatingActionButtonAnimator setAnimationDuration(long animationDuration) {
        this.animationDuration = animationDuration;
        return this;
    }

    public void setListener(OnAnimationParameterChange listener) {
        this.mOnAnimationParameterChange = listener;
    }

    @Override
    public void updateAnimations(ArrayList<MenuButtonArea> menuPoints, int gab) {
        //
    }

    @Override
    public void init(@NonNull View view) {
        this.view = view;

        mRadiusStartAnimation = ValueAnimator.ofFloat(START_ROTATION_RADIUS, END_ROTATION_RADIUS);
        mRadiusStartAnimation.setInterpolator(new LinearInterpolator());
        mRadiusStartAnimation.setDuration(animationDuration / 4);
        mRadiusStartAnimation.addUpdateListener(mRadiusUpdateListener);
        mRadiusStartAnimation.addListener(mRadiusAnimationListener);

        mRadiusEndAnimation = ValueAnimator.ofFloat(END_ROTATION_RADIUS, START_ROTATION_RADIUS);
        mRadiusEndAnimation.setInterpolator(new LinearInterpolator());
        mRadiusEndAnimation.setDuration(animationDuration / 4);
        mRadiusEndAnimation.addUpdateListener(mRadiusUpdateListener);

        mRotationAnimation = ValueAnimator.ofFloat(START_ROTATION_ANGLE, END_ROTATION_ANGLE);
        mRotationAnimation.setDuration(animationDuration);
        mRotationAnimation.setInterpolator(new AccelerateInterpolator());
        mRotationAnimation.addUpdateListener(mRotationUpdateListener);

        mRotationReverseAnimation = ValueAnimator.ofFloat(END_ROTATION_ANGLE, START_ROTATION_ANGLE);
        mRotationReverseAnimation.setDuration(animationDuration);
        mRotationReverseAnimation.setInterpolator(new AccelerateInterpolator());
        mRotationReverseAnimation.addUpdateListener(mRotationUpdateListener);
    }

    @Override
    public void clear() {
        mRadiusStartAnimation = null;
    }

    @Override
    public boolean startShowAnimate() {
        mRotationAnimation.start();
        return true;
    }

    @Override
    public boolean startHideAnimate() {
        mRotationReverseAnimation.start();
        return true;
    }

    @Override
    public void cancelAllAnimation() {
        mRotationAnimation.cancel();
        mRotationReverseAnimation.cancel();
    }

    Animator getBezierAnimation() {
        return mRadiusStartAnimation;
    }

    private ValueAnimator.AnimatorUpdateListener mRadiusUpdateListener = new ValueAnimator.AnimatorUpdateListener() {
        @Override
        public void onAnimationUpdate(ValueAnimator valueAnimator) {
            mOnAnimationParameterChange.radiusParameterUpdateListener((float) valueAnimator.getAnimatedValue());
            view.invalidate();
        }
    };

    private ValueAnimator.AnimatorUpdateListener mRotationUpdateListener = new ValueAnimator.AnimatorUpdateListener() {
        @Override
        public void onAnimationUpdate(ValueAnimator valueAnimator) {
            mOnAnimationParameterChange.rotationParameterUpdateListener((float) valueAnimator.getAnimatedValue());
            view.invalidate();
        }
    };

    private ValueAnimator.AnimatorListener mRadiusAnimationListener = new Animator.AnimatorListener() {
        @Override
        public void onAnimationStart(Animator animator) {

        }

        @Override
        public void onAnimationEnd(Animator animator) {
            mRadiusEndAnimation.start();
        }

        @Override
        public void onAnimationCancel(Animator animator) {

        }

        @Override
        public void onAnimationRepeat(Animator animator) {

        }
    };
}
