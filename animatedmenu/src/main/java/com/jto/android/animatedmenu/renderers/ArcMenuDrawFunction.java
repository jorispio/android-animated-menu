package com.jto.android.animatedmenu.renderers;

import android.graphics.Point;

import com.jto.android.animatedmenu.buttons.MenuButtonArea;

public class ArcMenuDrawFunction extends GenericMenuDrawFunction {

    public ArcMenuDrawFunction() {
        super();
        animator = new ArcMenuAnimator(this);
        setAngularRange(-Math.PI,  Math.PI);
    }

    @Override
    protected Point getButtonPosition(MenuButtonArea menuButtonArea) {
        float finalAngle = menuButtonArea.getAngle();
        float cursor = menuButtonArea.getParameter();
        float radius = menuButtonArea.getRadius();
        float angle = (float) Math.PI / 2f + (float)(finalAngle - Math.PI / 2f) * cursor;
        float currentRadius = getArcRadius(angle);
        float x = (float) (currentRadius * Math.cos(angle));
        float y = (float) (currentRadius * Math.sin(angle));
        return new Point((int)x, (int)y);
    }
}
